#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "AthContainers/ConstDataVector.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include <Hto4bLLPAlgorithm/Hto4bLLPFunctions.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODTruth/TruthVertex.h>
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "TFile.h"
#include "TEnv.h"
#include "TSystem.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <string>

namespace Hto4bLLPFunctions {

  Particles btagAssociatedTracks(const xAOD::Jet* jet, int jetIndex){

    const xAOD::BTagging *bjet(nullptr);
    bjet = jet->btagging();
    
    TrackLinks assocTracks = bjet->auxdata<TrackLinks>("BTagTrackToJetAssociator");
    Particles selectedTracks;
    
    for (unsigned int iT = 0; iT < assocTracks.size(); iT++) {
      const xAOD::TrackParticle *tmpTrk = *(assocTracks.at(iT));
      if (!assocTracks.at(iT).isValid()) {
	tmpTrk->auxdecor< int >("isAssoc")= 0;
	continue;
      }
      tmpTrk->auxdecor< int >("isAssoc") = 1;
      tmpTrk->auxdecor< int >("jetIndex") = jetIndex;
      selectedTracks.push_back(tmpTrk);
    }

    Particles tracks(selectedTracks.begin(), selectedTracks.end());
    return tracks;
  }


  bool isB(const xAOD::TruthParticle* p);
  std::vector<const xAOD::TruthParticle*> getBDecayChain(const xAOD::TruthParticle* p);

  bool isB(const xAOD::TruthParticle* p) {
    return (abs(p->pdgId()) % 1000 > 500) or (abs(p->pdgId())==5) or (abs(p->pdgId()) % 10000 > 5000);
  }
  
  std::vector<const xAOD::TruthParticle*> getBDecayChain(const xAOD::TruthParticle* p) {
    std::vector<const xAOD::TruthParticle*> chain;
    if (isB(p)){
      chain.push_back(p);
      int nChildren = p->nChildren();
      for( int i = 0; i < nChildren; ++i){
	int x=p->pdgId() * p->child(i)->pdgId();
	if ((250<x && x<25000) || x==-25 || (x>-5*5554 && x<-5*555)){ continue;}
	std::vector<const xAOD::TruthParticle*> newChain = getBDecayChain(p->child(i));
	chain.insert(chain.end(),newChain.begin(),newChain.end());
      }
    }
    return chain;
  }

  EL::StatusCode passCut(TH1D* cutflowHist, TH1D* cutflowHistW, int iCutflow, float eventWeight) {
    cutflowHist->Fill(iCutflow, 1);
    cutflowHistW->Fill(iCutflow, eventWeight);
    iCutflow++;
    return EL::StatusCode::SUCCESS;
  }




}

