#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "AthContainers/ConstDataVector.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include <Hto4bLLPAlgorithm/Hto4bLLPFilter.h>
#include <Hto4bLLPAlgorithm/Hto4bLLPFunctions.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODTruth/TruthVertex.h>
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "TFile.h"
#include "TEnv.h"
#include "TSystem.h"
#include "TLorentzVector.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static float GeV = 1000.;


// this is needed to distribute the algorithm to the workers
ClassImp(Hto4bLLPFilter)

Hto4bLLPFilter :: Hto4bLLPFilter () : Algorithm ( "Hto4bLLPFilter" )
{
// Here you put any code for the base initialization of variables,
// e.g. initialize all pointers to 0.  Note that you should only put
// the most basic initialization here, since this method will be
// called on both the submission and the worker node.  Most of your
// initialization code will go into histInitialize() and
// initialize().

  ANA_MSG_INFO("Hto4bLLPFilter() : Calling constructor");

  m_allJetContainerName      = "";
  m_inMuContainerName        = "";
  m_inElContainerName        = "";
  m_msgLevel                 = MSG::INFO;
  m_TrackMinPt               = 0;
  m_TrackZ0Max               = 0;
  m_TrackD0Max               = 0;
  m_jetPtCut                 = 0;
  m_AlphaMaxCut              = 0;
  m_CHFCut                   = 0;
  m_electronPtCut            = 0;
  m_muonPtCut                = 0;
}



EL::StatusCode  Hto4bLLPFilter :: configure ()
{
  ANA_MSG_INFO("configure() : Configuring Hto4bLLPFilter Interface.");

  ANA_MSG_INFO("configure() : Hto4bLLPFilter Interface succesfully configured! \n");

  if( m_allJetContainerName.empty() ) {
    ANA_MSG_ERROR("configure() : InputContainer string is empty!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode Hto4bLLPFilter :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  xAOD::Init( "Hto4bLLPFilter" ).ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode Hto4bLLPFilter :: histInitialize ()
{
  // Here you do everything that needs to be done at the very                                                                                                                        
  // beginning on each worker node, e.g. create histograms and output                                                                                                                
  // trees.  This method gets called before any input files are                                                                                                                      
  // connected.                                                                                                                                                                      
  ANA_MSG_INFO("histInitialize() : Calling histInitialize \n");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPFilter :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every                                                                                                             
  // single file, e.g. collect a list of all lumi-blocks processed                                                                                                                   
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Hto4bLLPFilter :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,                                                                                                               
  // e.g. resetting branch addresses on trees.  If you are using                                                                                                                     
  // D3PDReader or a similar service this method is not needed.                                                                                                                      
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Hto4bLLPFilter :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  m_eventCounter = -1;

  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK (HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  if ( this->configure() == EL::StatusCode::FAILURE ) {
    ANA_MSG_ERROR("initialize() : Failed to properly configure. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Hto4bLLPFilter :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  //cout<<"Hto4b execute"<<endl;

  ANA_MSG_DEBUG("execute(): Applying selection");
  ++m_eventCounter;


  //----------------------------
  // Event information
  //---------------------------

  ///////////////////////////// Retrieve Containers /////////////////////////////////////////

  ANA_MSG_DEBUG("execute() : Get Containers");
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK (HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  const xAOD::VertexContainer* vertices = 0;
  ANA_CHECK (HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store));

  const xAOD::MuonContainer* allMuons = 0;
  const xAOD::ElectronContainer* allElectrons = 0;
  ANA_CHECK (HelperFunctions::retrieve(allMuons, m_inMuContainerName, m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(allElectrons, m_inElContainerName, m_event, m_store));

  bool pass(false);

  const xAOD::JetContainer* allJets = 0;
  ANA_CHECK (HelperFunctions::retrieve(allJets, m_allJetContainerName, m_event, m_store));

  pass = this->executeFilter( eventInfo, allJets, allMuons, allElectrons, vertices);

    return EL::StatusCode::SUCCESS;

}



bool Hto4bLLPFilter :: executeFilter ( const xAOD::EventInfo* eventInfo,
    const xAOD::JetContainer* allJets,
    const xAOD::MuonContainer* allMuons,
    const xAOD::ElectronContainer* allElectrons,
    const xAOD::VertexContainer* vertices) {

  /////////////////////////// Begin Selections  ///////////////////////////////

  //int evtNum = eventInfo->eventNumber();

  int passesFilter=0;
  bool passesJet=false;
  bool passesEl=false;
  bool passesMu=false;


  // New Containers for Jets, Electrons and Muons

  newJetContainers(allJets, allElectrons);
  const xAOD::JetContainer* good_jets;
  const xAOD::JetContainer* selected_jets;
  const xAOD::JetContainer* signal_jets;
  ANA_CHECK (HelperFunctions::retrieve(good_jets, "good_jets", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_jets, "selected_jets", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(signal_jets, "signal_jets", m_event, m_store));

  newElectronContainers(allElectrons, eventInfo, vertices);

  const xAOD::ElectronContainer* good_electrons;
  const xAOD::ElectronContainer* selected_electrons;
  ANA_CHECK (HelperFunctions::retrieve(good_electrons, "good_electrons", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_electrons, "selected_electrons", m_event, m_store));

  newMuonContainers(allMuons, eventInfo, vertices);

  const xAOD::MuonContainer* good_muons;
  const xAOD::MuonContainer* selected_muons;
  ANA_CHECK (HelperFunctions::retrieve(good_muons, "good_muons", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_muons, "selected_muons", m_event, m_store));

  if (selected_electrons->size()>0) {passesEl = true;}
  if (selected_muons->size()>0) {passesMu = true;}


  int nJets=0;


  //Calculating alpha max and charged hadron fraction
  int j=0;
  for (auto jet : *signal_jets){
    j++;
    TLorentzVector VJet = TLorentzVector(0.0,0.0,0.0,0.0);
    VJet.SetPtEtaPhiE(jet->pt(), jet->eta(), jet->phi(), jet->e());

    TLorentzVector CHFNum = TLorentzVector(0.0,0.0,0.0,0.0);

    int jetIndex = j-1;

    Hto4bLLPFunctions::Particles associated_tracks = Hto4bLLPFunctions::btagAssociatedTracks(jet, jetIndex);

    float alpha_max=-9;
    int i=0;

     for ( auto vertex : *vertices) {
          TLorentzVector alphaDen = TLorentzVector(0.0,0.0,0.0,0.0);
	  TLorentzVector alphaNum = TLorentzVector(0.0,0.0,0.0,0.0);
	  float alpha;
	  for( auto track : associated_tracks) {
	    if (track->pt() < m_TrackMinPt) continue;
	    TLorentzVector VTrack = TLorentzVector(0.0,0.0,0.0,0.0);
	    VTrack.SetPtEtaPhiE(track->pt(),track->eta(), track->phi(), track->e());
	    alphaDen=alphaDen+VTrack;
            if (track->d0() > m_TrackD0Max) continue;
	    float z0 = track->z0() + track->vz() - vertex->z();
	    float theta = track->theta();
	    if (fabs(z0*sin(theta)) < m_TrackZ0Max ) {
	       alphaNum=alphaNum+VTrack;
		}
	  }
	  if (alphaDen.Pt()==0){alpha=-99;}
	  else {
	  alpha = alphaNum.Pt()/alphaDen.Pt();
	  }
	  if (alpha > alpha_max) { alpha_max=alpha; }
	  i++;
     }

     for (auto track : associated_tracks){
       TLorentzVector VTrack = TLorentzVector(0.0,0.0,0.0,0.0);
       if (track->d0() > m_TrackD0Max) continue;
       if (track->pt() < m_TrackMinPt) continue;
       VTrack.SetPtEtaPhiE(track->pt(),track->eta(), track->phi(), track->e());
       CHFNum = CHFNum + VTrack;
     }

     float chf = CHFNum.Pt()/VJet.Pt();
     jet->auxdecor< float >("chf") = chf;
     jet->auxdecor< float >("alpha_max") = alpha_max;
     if (j<=2){
       jet->auxdecor< int >("alpha_max_pass") = (alpha_max < m_AlphaMaxCut)*1;
       jet->auxdecor< int >("chf_pass") = (chf < m_CHFCut)*1;
     }
     else {
       jet->auxdecor< int >("alpha_max_pass") = -9;
       jet->auxdecor< int >("chf_pass") = -9;
     }

     if (((chf < m_CHFCut) || (alpha_max < m_AlphaMaxCut)) && j<=2){nJets++;}

  }
  if (nJets>0){passesJet=true;}


  eventInfo->auxdecor< int >("passesMuonFilter") = passesMu;
  eventInfo->auxdecor< int >("passesElecFilter") = passesEl;

  if (passesJet && (passesEl || passesMu)){
    passesFilter=1;
  }
  eventInfo->auxdecor< int >("passesFilter")= passesFilter;

  return true;
}


void Hto4bLLPFilter::newJetContainers(const xAOD::JetContainer* input, const xAOD::ElectronContainer* electrons){

  ConstDataVector<xAOD::JetContainer>* goodJets(nullptr);
  ConstDataVector<xAOD::JetContainer>* selectedJets(nullptr);
  ConstDataVector<xAOD::JetContainer>* signalJets(nullptr);
  goodJets = new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
  selectedJets = new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
  signalJets = new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);

  for (auto jet: *input){
    int good = 0;
    int selected = 0;
    int signal = 0;
    TLorentzVector VJet = jet->p4();
    float minDR = 100;

    for (auto elec: *electrons){

      if (elec->pt()<20000) continue;
      if (fabs(elec->eta())>2.47) continue;
      bool passLoose=false;
      if (!elec->passSelection(passLoose, "LHLoose")){continue;}
      if (!passLoose) continue;

      TLorentzVector VElectron = elec->p4();
      float DR = VElectron.DeltaR(VJet);

      if (DR < minDR) {minDR=DR;}
    }
    if (minDR > 0.2){
      goodJets->push_back(jet);
      good = 1;

      if (jet->pt()>20000){

	if (fabs(jet->eta())<2.8){
	  selectedJets->push_back(jet);
	  selected = 1;
	}

	if (fabs(jet->eta())<2.1){
	  signalJets->push_back(jet);
	  signal = 1;
	}
      }
    }
    jet->auxdecor<int>( "good_jet" ) = good;
    jet->auxdecor<int>( "selected_jet" ) = selected;
    jet->auxdecor<int>( "signal_jet" ) = signal;
  }
  m_store->record( goodJets, "good_jets" );
  m_store->record( selectedJets, "selected_jets" );
  m_store->record( signalJets, "signal_jets" );
}

void Hto4bLLPFilter::newElectronContainers(const xAOD::ElectronContainer* input, const xAOD::EventInfo* eventInfo, const xAOD::VertexContainer* vertices){

  ConstDataVector<xAOD::ElectronContainer>* goodElectrons(nullptr);
  ConstDataVector<xAOD::ElectronContainer>* selectedElectrons(nullptr);
  goodElectrons = new ConstDataVector<xAOD::ElectronContainer>(SG::VIEW_ELEMENTS);
  selectedElectrons = new ConstDataVector<xAOD::ElectronContainer>(SG::VIEW_ELEMENTS);

  for (auto elec: *input){
    int good = 0;
    int selected = 0;

    if (elec->passSelection("LHMedium") && elec->isolation(xAOD::Iso::topoetcone20)/elec->pt()<0.2){
      goodElectrons->push_back(elec);
      good = 1;

      for (xAOD::VertexContainer::const_iterator vxIter2 = vertices->begin(); vxIter2 != vertices->end(); ++vxIter2) {
	// Select good primary vertex                                                                                                                                              
	if ((*vxIter2)->vertexType() == xAOD::VxType::PriVtx) {
	  const xAOD::TrackParticle* tp = elec->trackParticle() ; //your input track particle from the electron                                                                    
	  float sigd0 =fabs( xAOD::TrackingHelpers::d0significance( tp, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() ));
	  double delta_z0 = tp->z0() + tp->vz() - (*vxIter2)->z();
	  double theta = tp->theta();
	  float z0sintheta = fabs(delta_z0 * sin(theta));

	  if( (fabs(elec->caloCluster()->etaBE(2))>1.37 && fabs(elec->caloCluster()->etaBE(2)<1.52)) || fabs(elec->caloCluster()->etaBE(2))>2.47 ) continue;

	  if ( (sigd0<5 && z0sintheta<0.5 && elec->pt()>m_electronPtCut)){ 
	    selectedElectrons->push_back(elec);
	    selected = 1;
	  }
	}
      }
    }
    elec->auxdecor<int>( "good_electron" ) = good;
    elec->auxdecor<int>( "selected_electron" ) = selected;
  }
  m_store->record( goodElectrons, "good_electrons" );
  m_store->record( selectedElectrons, "selected_electrons" );
}

void Hto4bLLPFilter::newMuonContainers(const xAOD::MuonContainer* input, const xAOD::EventInfo* eventInfo, const xAOD::VertexContainer* vertices){

  ConstDataVector<xAOD::MuonContainer>* goodMuons(nullptr);
  ConstDataVector<xAOD::MuonContainer>* selectedMuons(nullptr);
  goodMuons = new ConstDataVector<xAOD::MuonContainer>(SG::VIEW_ELEMENTS);
  selectedMuons = new ConstDataVector<xAOD::MuonContainer>(SG::VIEW_ELEMENTS);

  for (auto muon: *input){
    int good = 0;
    int selected = 0;

    if( !m_muonSelectionTool_handle->passedMuonCuts(*muon) ) continue;
    if (muon->muonType() != xAOD::Muon::Combined) continue;
    if (muon->isolation(xAOD::Iso::topoetcone20)/muon->pt()<0.3) {
      goodMuons->push_back(muon);
      good = 1;
      for (xAOD::VertexContainer::const_iterator vxIter3 = vertices->begin(); vxIter3 != vertices->end(); ++vxIter3) {
	// Select good primary vertex                                                                                                                                              
	if ((*vxIter3)->vertexType() == xAOD::VxType::PriVtx) {
	  const xAOD::TrackParticle* tp = muon->primaryTrackParticle() ; //your input track particle from the muon                                                                 
	  double d0sig = xAOD::TrackingHelpers::d0significance( tp, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
	  float delta_z0 = tp->z0() + tp->vz() - (*vxIter3)->z();
	  float theta = tp->theta();
	  double z0sintheta = delta_z0 * sin(theta);

	  if(muon->pt()>m_muonPtCut && fabs(muon->eta())<2.5 && fabs(d0sig)<3 && fabs(z0sintheta)<0.5){
	    selectedMuons->push_back(muon);
	    selected = 1;
	  }
	}
      }
    }
    muon->auxdecor<int>( "good_muon" ) = good;
    muon->auxdecor<int>( "selected_muon" ) = selected;
  }
  m_store->record( goodMuons, "good_muons" );
  m_store->record( selectedMuons, "selected_muons" );
}




EL::StatusCode Hto4bLLPFilter :: postExecute ()
{
  // Here you do everything that needs to be done after the main event                                                                                                               
  // processing.  This is typically very rare, particularly in user                                                                                                                  
  // code.  It is mainly used in implementing the NTupleSvc.                                                                                                                         
  return EL::StatusCode::SUCCESS;
}




EL::StatusCode Hto4bLLPFilter :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Hto4bLLPFilter :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it                                                                                                                 
  // gets called after the last event has been processed on the worker                                                                                                               
  // node and allows you to finish up any objects you created in                                                                                                                     
  // histInitialize() before they are written to disk.  This is                                                                                                                      
  // actually fairly rare, since this happens separately for each                                                                                                                    
  // worker node.  Most of the time you want to do your                                                                                                                              
  // post-processing on the submission node after all your histogram                                                                                                                 
  // outputs have been merged.  This is different from finalize() in                                                                                                                 
  // that it gets called on all worker nodes regardless of whether                                                                                                                   
  // they processed input events.  

  return EL::StatusCode::SUCCESS;
}
