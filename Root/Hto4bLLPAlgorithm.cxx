#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "AthContainers/ConstDataVector.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include <Hto4bLLPAlgorithm/Hto4bLLPAlgorithm.h>
#include <Hto4bLLPAlgorithm/Hto4bLLPFunctions.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODTruth/TruthVertex.h>
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "TFile.h"
#include "TEnv.h"
#include "TSystem.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static float GeV = 1000.;


// this is needed to distribute the algorithm to the workers
ClassImp(Hto4bLLPAlgorithm)

Hto4bLLPAlgorithm :: Hto4bLLPAlgorithm () :
  m_cutflowHist(0),
  m_cutflowHistW(0)
{
// Here you put any code for the base initialization of variables,
// e.g. initialize all pointers to 0.  Note that you should only put
// the most basic initialization here, since this method will be
// called on both the submission and the worker node.  Most of your
// initialization code will go into histInitialize() and
// initialize().

  ANA_MSG_INFO("Hto4bLLPAlgorithm() : Calling constructor");

  m_inJetContainerName       = "";
  m_inputAlgo                = "";
  m_allJetContainerName      = "";
  m_allJetInputAlgo          = "";
  m_inMETContainerName       = "";
  m_inMETTrkContainerName    = "";
  m_msgLevel                 = MSG::INFO;
  m_useCutFlow               = true;
  m_MCPileupCheckContainer   = "AntiKt4TruthJets";
  m_leadingJetPtCut          = 225;
  m_subleadingJetPtCut       = 225;
  m_jetMultiplicity          = 3;
  m_truthLevelOnly           = false;
  m_metCut                   = 0;
}




EL::StatusCode  Hto4bLLPAlgorithm :: configure ()
{
  ANA_MSG_INFO("configure() : Configuring Hto4bLLPAlgorithm Interface.");

  if( m_MCPileupCheckContainer == "None" ) {
    m_useMCPileupCheck = false;
  }

  m_comEnergy = "13TeV";

  ANA_MSG_INFO("configure() : Hto4bLLPAlgorithm Interface succesfully configured! \n");

  if( m_inJetContainerName.empty() ) {
    ANA_MSG_ERROR("configure() : InputContainer string is empty!");
    return EL::StatusCode::FAILURE;
  }

  if( !m_truthLevelOnly &&  m_allJetContainerName.empty() ) {
    ANA_MSG_ERROR("configure() : AllJetInputContainer string is empty!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode Hto4bLLPAlgorithm :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  xAOD::Init( "Hto4bLLPAlgorithm" ).ignore(); // call before opening first file

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPAlgorithm :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ANA_MSG_INFO("histInitialize() : Calling histInitialize \n");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPAlgorithm :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPAlgorithm :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPAlgorithm :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  m_eventCounter = -1;

  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK (HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  if ( this->configure() == EL::StatusCode::FAILURE ) {
    ANA_MSG_ERROR("initialize() : Failed to properly configure. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  // m_truthLevelOnly is set in config so need to do this after configure is called
  if( m_truthLevelOnly ) { m_isMC = true; }
  else {
    m_isMC = ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) ? true : false;
  }

  getLumiWeights(eventInfo);

  if(m_useCutFlow) {

    TFile *file = wk()->getOutputFile ("cutflow");
    m_cutflowHist  = (TH1D*)file->Get("cutflow");
    m_cutflowHistW = (TH1D*)file->Get("cutflow_weighted");

    m_cutflowFirst = m_cutflowHist->GetXaxis()->FindBin("TriggerEfficiency");
    m_cutflowHistW->GetXaxis()->FindBin("TriggerEfficiency");

    if(m_useMCPileupCheck && m_isMC){
      m_cutflowHist->GetXaxis()->FindBin("mcCleaning");
      m_cutflowHistW->GetXaxis()->FindBin("mcCleaning");
    }

    m_cutflowHist->GetXaxis()->FindBin("y*");
    m_cutflowHistW->GetXaxis()->FindBin("y*");

  }
  ANA_MSG_INFO("initialize() : Successfully initialized! \n");
  return EL::StatusCode::SUCCESS;
}


void Hto4bLLPAlgorithm::AddHists( std::string name ) {

  std::string jhName("highPtJets");
  //if( ! name.empty() ) { jhName += "."; } // makes it hard to do command line stuff
  jhName += name; // add systematic

}


EL::StatusCode Hto4bLLPAlgorithm :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  //cout<<"Hto4b execute"<<endl;

  ANA_MSG_DEBUG("execute(): Applying selection");
  ++m_eventCounter;

  m_iCutflow = m_cutflowFirst;

  //----------------------------
  // Event information
  //---------------------------

  ///////////////////////////// Retrieve Containers /////////////////////////////////////////

  ANA_MSG_DEBUG("execute() : Get Containers");
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK (HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  if (m_eventCounter == 0 && m_isMC) {
    getLumiWeights(eventInfo);
  } else if (!m_isMC) {
    m_filtEff = m_xs = 1.0;
  }

  SG::AuxElement::ConstAccessor<float> NPVAccessor("NPV");
  const xAOD::VertexContainer* vertices = 0;
  if(!m_truthLevelOnly) {
    ANA_CHECK (HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store));
  }
  if(!m_truthLevelOnly && !NPVAccessor.isAvailable( *eventInfo )) { // NPV might already be available
    // number of PVs with 2 or more tracks
    //eventInfo->auxdecor< int >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
    // TMP for JetUncertainties uses the same variable
    eventInfo->auxdecor< float >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
  }

  const xAOD::TrackParticleContainer* tracks = 0;
  ANA_CHECK (HelperFunctions::retrieve(tracks, "InDetTrackParticles", m_event, m_store));

  const xAOD::TruthParticleContainer* TruthPart = nullptr;
  if (m_isMC){ANA_CHECK (HelperFunctions::retrieve(TruthPart, "TruthParticles", m_event, m_store));}
 
  const xAOD::MissingETContainer* Met(0);
  ANA_CHECK (HelperFunctions::retrieve(Met, m_inMETContainerName, m_event, m_store));

  const xAOD::MissingETContainer* MetTrk(0);
  ANA_CHECK (HelperFunctions::retrieve(MetTrk, m_inMETTrkContainerName, m_event, m_store));

  //#####################################################################################################################################################
  const xAOD::JetContainer* truthJets = 0;
  if(m_useMCPileupCheck && m_isMC){
    ANA_CHECK (HelperFunctions::retrieve(truthJets, m_MCPileupCheckContainer, m_event, m_store));
  }

  //Set this first, as m_mcEventWeight is needed by passCut()
  if(m_isMC)
    m_mcEventWeight = eventInfo->mcEventWeight();
  else
    m_mcEventWeight = 1;

  //float weight_pileup = 1.;
  //if( m_isMC && eventInfo->isAvailable< float >( "PileupWeight") )
  //  weight_pileup = eventInfo->auxdecor< float >("PileupWeight");

  eventInfo->auxdecor< float >("weight_xs") = m_xs * m_filtEff;
  eventInfo->auxdecor< float >("weight") = m_mcEventWeight * m_xs * m_filtEff;

  const xAOD::MuonContainer* allMuons = 0;
  const xAOD::ElectronContainer* allElectrons = 0;
  ANA_CHECK (HelperFunctions::retrieve(allMuons, m_inMuContainerName, m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(allElectrons, m_inElContainerName, m_event, m_store));


  //%%%%%%%%%%%%%%%%%%%%%%%%%%%% Loop over Systematics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ANA_MSG_DEBUG("execute() : Systematic Loop");

  // did any collection pass the cuts?
  bool pass(false);
  bool doCutflow(m_useCutFlow); // will only stay true for nominal
  const xAOD::JetContainer* signalJets = 0;
  const xAOD::JetContainer* allJets = 0;

  // if input comes from xAOD, or just running one collection,
  // then get the one collection and be done with it
  if( m_inputAlgo == "" || m_truthLevelOnly ) {
    ANA_CHECK (HelperFunctions::retrieve(signalJets, m_inJetContainerName, m_event, m_store));
    if(!m_truthLevelOnly) { ANA_CHECK (HelperFunctions::retrieve(allJets, m_allJetContainerName, m_event, m_store)); }

    // executeAnalysis
    pass = this->executeAnalysis( eventInfo, signalJets, truthJets, allJets, allMuons, allElectrons, vertices, tracks, TruthPart, Met, MetTrk, doCutflow, "" );

  }
  else { // get the list of systematics to run over

    // get vector of string giving the names
    std::vector<std::string>* systNames = 0;
    if ( m_store->contains< std::vector<std::string> >( m_inputAlgo ) ) {
      if(!m_store->retrieve( systNames, m_inputAlgo ).isSuccess()) {
        ANA_MSG_INFO("execute() : Cannot find vector from "<<m_inputAlgo.c_str());
        return StatusCode::FAILURE;
      }
    }


    // loop over systematics
    bool saveContainerNames(false);
    std::vector< std::string >* vecOutContainerNames = 0;
    if(saveContainerNames) { vecOutContainerNames = new std::vector< std::string >; }
    // should only doCutflow for the nominal
    bool passOne(false);
    std::string inContainerName("");
    std::string allJetContainerName("");
    if (systNames->size() !=0){
      for( auto systName : *systNames ) {
	ANA_MSG_DEBUG("execute() : Systematic Loop"<<systName);
	inContainerName = m_inJetContainerName+systName;
	ANA_CHECK (HelperFunctions::retrieve(signalJets, inContainerName, m_event, m_store));
	//
	allJetContainerName = m_allJetContainerName+systName;
	ANA_CHECK (HelperFunctions::retrieve(allJets, allJetContainerName, m_event, m_store));
	//
	// allign with Dijet naming conventions
	if( systName.empty() ) { doCutflow = m_useCutFlow; } // only doCutflow for nominal
	else { doCutflow = false; }
	passOne = this->executeAnalysis( eventInfo, signalJets, truthJets, allJets, allMuons, allElectrons, vertices, tracks, TruthPart, Met, MetTrk, doCutflow, systName );
	// save the string if passing the selection
	if( saveContainerNames && passOne ) { vecOutContainerNames->push_back( systName ); }
	// the final decision - if at least one passes keep going!
	pass = pass || passOne;

      }
    }
    // save list of systs that should be considered down stream
    if( saveContainerNames ) {
      ANA_CHECK (m_store->record( vecOutContainerNames, m_name));
    }
  }

  //if(!pass) {
  //wk()->skipEvent();
  //}
  return EL::StatusCode::SUCCESS;

}



bool Hto4bLLPAlgorithm :: executeAnalysis ( const xAOD::EventInfo* eventInfo,
    const xAOD::JetContainer* signalJets,
    const xAOD::JetContainer* truthJets,
    const xAOD::JetContainer* allJets,
    const xAOD::MuonContainer* allMuons,
    const xAOD::ElectronContainer* allElectrons,
    const xAOD::VertexContainer* vertices,
    const xAOD::TrackParticleContainer* tracks,
    const xAOD::TruthParticleContainer* TruthPart,
    const xAOD::MissingETContainer* Met,
    const xAOD::MissingETContainer* MetTrk,
    bool doCutflow,
    std::string systName) {

  /////////////////////////// Begin Selections  ///////////////////////////////

  int evtNum = eventInfo->eventNumber();

  ANA_MSG_DEBUG("executeAnalysis");

  //std::cout<<"Ambers filter started. Event number is "<<evtNum<<std::endl;  

  /*if(m_isMC){
    
    }*/
  ////  leadingJetPt trigger efficiency BEFORE MCCLEANING ////

  if (signalJets->size() !=0 ){
    if( signalJets->at(0)->pt() < m_leadingJetPtCut || signalJets->size() < m_jetMultiplicity){
      wk()->skipEvent();  return EL::StatusCode::SUCCESS;
    }
    if(doCutflow) Hto4bLLPFunctions::passCut(m_cutflowHist, m_cutflowHistW, m_iCutflow, m_mcEventWeight); //TriggerEfficiency
  
    if( signalJets->at(1)->pt() < m_subleadingJetPtCut || signalJets->size() < m_jetMultiplicity){
      wk()->skipEvent();  return EL::StatusCode::SUCCESS;
    }
    if(doCutflow) Hto4bLLPFunctions::passCut(m_cutflowHist, m_cutflowHistW, m_iCutflow, m_mcEventWeight); //TriggerEfficiency
  }
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End Selections %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ///////////////////////// Add final variables ////////////////////////////////
  // why decorate even if might not be filling tree?
  // if are not set here, miniTree puts -999 since not available
  if(signalJets->size() >= 3) {
    eventInfo->auxdecor< float >( "m3j" ) = ( signalJets->at(0)->p4() + signalJets->at(1)->p4() + signalJets->at(2)->p4()).M() / GeV;
  }

   if( signalJets->size() > 1 ) {
    const xAOD::Jet* leadJet     = signalJets->at(0);
    const xAOD::Jet* subLeadJet  = signalJets->at(1);
    float mjj = ( leadJet->p4() + subLeadJet->p4() ).M() / GeV;
    eventInfo->auxdecor< float >( "mjj" ) = mjj;

    TLorentzVector Vmnj = TLorentzVector(0.0,0.0,0.0,0.0);
    for (unsigned int i=0; i<signalJets->size(); i++){
      if (i==4) break;
      Vmnj = Vmnj + signalJets->at(i)->p4();
    }
    float mnj = Vmnj.M() / GeV;
    eventInfo->auxdecor< float >( "mnj" ) = mnj;
   }

  //Amber's Addition################################################################################################################################################

  TLorentzVector hiPtLepton = TLorentzVector(0.0,0.0,0.0,0.0);
  TLorentzVector lowCHFJet = TLorentzVector(0.0,0.0,0.0,0.0);
  TLorentzVector sumLeadingJets = TLorentzVector(0.0,0.0,0.0,0.0);

  float CHFMin = 100;

  const xAOD::MissingET* final_clus = *Met->find("FinalClus");
  const xAOD::MissingET* final_trk = *MetTrk->find("FinalTrk");

  TVector2 clus = TVector2(0.0,0.0);
  TVector2 trk  = TVector2(0.0,0.0);

  clus.SetMagPhi(final_clus->met(),final_clus->phi());
  trk.SetMagPhi(final_trk->met(), final_trk->phi());
 
  eventInfo->auxdecor< float >("metDPhi") = clus.DeltaPhi(trk);

  const xAOD::JetContainer* good_jets;
  const xAOD::JetContainer* selected_jets;
  const xAOD::JetContainer* signal_jets;
  ANA_CHECK (HelperFunctions::retrieve(good_jets, "good_jets", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_jets, "selected_jets", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(signal_jets, "signal_jets", m_event, m_store));

  const xAOD::ElectronContainer* good_electrons;
  const xAOD::ElectronContainer* selected_electrons;
  ANA_CHECK (HelperFunctions::retrieve(good_electrons, "good_electrons", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_electrons, "selected_electrons", m_event, m_store));

  const xAOD::MuonContainer* good_muons;
  const xAOD::MuonContainer* selected_muons;
  ANA_CHECK (HelperFunctions::retrieve(good_muons, "good_muons", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_muons, "selected_muons", m_event, m_store));

  ANA_MSG_DEBUG("Leptons");

  if ( selected_muons->size()>0 && selected_electrons->size()>0 ){
    if ( selected_muons->at(0)->pt() > selected_electrons->at(0)->pt()){ hiPtLepton = selected_muons->at(0)->p4();}
    else { hiPtLepton = selected_electrons->at(0)->p4();}
  }
  else if ( selected_muons->size()>0 ) { hiPtLepton = selected_muons->at(0)->p4();}
  else if ( selected_electrons->size()>0) { hiPtLepton = selected_electrons->at(0)->p4();}


  int nJets=0;

  ANA_MSG_DEBUG("Jets");

  for( auto jet : *allJets){
    TLorentzVector VJet = TLorentzVector(0.0,0.0,0.0,0.0);
    VJet.SetPtEtaPhiE(jet->pt(), jet->eta(), jet->phi(), jet->e());
    float LooseDR=100;
    float LooseDPhi=100;
    float MediumDR=100;
    float MediumDPhi=100;
    float TightDR=100;
    float TightDPhi=100;
    for (auto elec : *allElectrons){
      if (elec->isolation(xAOD::Iso::topoetcone20)/elec->pt()>0.2){ continue;}
      TLorentzVector VElec=elec->p4();
      //loose
      if (elec->passSelection("LHLoose")){
	float dR=VJet.DeltaR(VElec);
	float dPhi=VJet.DeltaPhi(VElec);
	if (dR<LooseDR){LooseDR=dR;}
	if (fabs(dPhi)<fabs(LooseDPhi)){LooseDPhi=dPhi;}
      }
      //medium
      if (elec->passSelection("LHMedium")){
        float dR=VJet.DeltaR(VElec);
	float dPhi=VJet.DeltaPhi(VElec);
	if (dR<MediumDR){MediumDR=dR;}
	if (fabs(dPhi)<fabs(MediumDPhi)){MediumDPhi=dPhi;}
      } 
      //tight
      if (elec->passSelection("LHTight")){
        float dR=VJet.DeltaR(VElec);
	float dPhi=VJet.DeltaPhi(VElec);
	if (dR<TightDR){TightDR=dR;}
	if (fabs(dPhi)<fabs(TightDPhi)){TightDPhi=dPhi;}
      } 
    }
    jet->auxdecor< float >("LooseDR")=LooseDR;
    jet->auxdecor< float >("LooseDPhi")=LooseDPhi;
    jet->auxdecor< float >("MediumDR")=MediumDR;
    jet->auxdecor< float >("MediumDPhi")=MediumDPhi;
    jet->auxdecor< float >("TightDR")=TightDR;
    jet->auxdecor< float >("TightDPhi")=TightDPhi;
  }

  if (signal_jets->size() > 0){

    int j=0;
    for (auto jet : *signal_jets){
      if (j<4) sumLeadingJets = sumLeadingJets + jet->p4();
      j+=1;

      TLorentzVector VJet = TLorentzVector(0.0,0.0,0.0,0.0);
      VJet.SetPtEtaPhiE(jet->pt(), jet->eta(), jet->phi(), jet->e());

      TLorentzVector VtTrack = TLorentzVector(0.0,0.0,0.0,0.0);

      Hto4bLLPFunctions::Particles associated_tracks = Hto4bLLPFunctions::btagAssociatedTracks(jet,j-1);

      float track_pt_max=-999;
      for (auto track : associated_tracks){
	float track_pt = track->pt();
	if (track_pt>track_pt_max){track_pt_max=track_pt;}
      }
      
      jet->auxdecor< float >("track_pt_max") = track_pt_max;
      jet->auxdecor< int >("ntracks") = associated_tracks.size();
      
      static SG::AuxElement::ConstAccessor<float> chf ("chf");
      
      if (chf(*jet) < CHFMin) {
	lowCHFJet = jet->p4();
	CHFMin = chf(*jet);
      }
    }
  }
  eventInfo->auxdecor< float >("lepJetMinCHFDeltaPhi") = hiPtLepton.DeltaPhi(lowCHFJet);
  eventInfo->auxdecor< float >("lepSumLeadingJetsDeltaPhi") = hiPtLepton.DeltaPhi(sumLeadingJets);
  
  TVector2 lep = TVector2(0.0,0.0);
  TVector2 vec = TVector2(0.0,0.0);
  TVector2 jet = TVector2(0.0,0.0);
  TVector2 sum = TVector2(0.0,0.0);

  lep.SetX(hiPtLepton.X()); lep.SetY(hiPtLepton.Y());
  jet.SetX(lowCHFJet.X()); jet.SetY(lowCHFJet.Y());
  sum.SetX(sumLeadingJets.X()); sum.SetY(sumLeadingJets.Y());

  vec = lep + clus;

  eventInfo->auxdecor< float >("vecJetMinCHFDeltaPhi") = vec.DeltaPhi(jet);
  eventInfo->auxdecor< float >("vecSumLeadingJetsDeltaPhi") = vec.DeltaPhi(sum);

  ANA_MSG_DEBUG("Truth");

  //pdgid stuff
  std::vector<const xAOD::TruthParticle*> LLP_bs;
  int i=0;
  float tbeta=-999;
  float tgamma=-999;
  float tR_coord=-999; 
  float tX_coord=-999;
  float tY_coord=-999;
  float tZ_coord=-999;
  float tlifetime_dil=-999;
  float tlifetime_prop= -999;
  TVector2 VNeutrino = TVector2(0.0,0.0);
  bool realV = false;
  if (m_isMC){
    for( auto p : *TruthPart) {
      if (p->child(0) != nullptr && p->child(1) != nullptr){
	if ((abs(p->pdgId()) == 23 || abs(p->pdgId()) == 24) && !realV){
	  if (abs(p->child(0)->pdgId()) == 11 || abs(p->child(0)->pdgId()) == 13 || abs(p->child(0)->pdgId()) == 15){
	    eventInfo->auxdecor< int >("Vchild") = p->child(0)->pdgId();
	    eventInfo->auxdecor< float >("VchildPt") = p->child(0)->pt();
	    eventInfo->auxdecor< int >("VId") = p->pdgId();
	    eventInfo->auxdecor< float >("VPt") = p->pt();
	    eventInfo->auxdecor< float >("VEta") = p->eta();
	    eventInfo->auxdecor< float >("VPhi") = p->phi();
	    eventInfo->auxdecor< float >("VMass") = p->m();
	    realV=true;
	  }
	  else if (abs(p->child(1)->pdgId()) == 11 || abs(p->child(1)->pdgId()) == 13 || abs(p->child(1)->pdgId()) == 15){
	    eventInfo->auxdecor< int >("Vchild") = p->child(1)->pdgId();
            eventInfo->auxdecor< float >("VchildPt") = p->child(1)->pt();
	    eventInfo->auxdecor< int >("VId") = p->pdgId();
	    eventInfo->auxdecor< float >("VPt") = p->pt();
	    eventInfo->auxdecor< float >("VEta") = p->eta();
	    eventInfo->auxdecor< float >("VPhi") = p->phi();
	    eventInfo->auxdecor< float >("VMass") = p->m();
	    realV=true;
	  }
          if (abs(p->child(0)->pdgId()) == 12 || abs(p->child(0)->pdgId()) == 14 || abs(p->child(0)->pdgId()) == 16){
            eventInfo->auxdecor< int >("NeutrinoId") = p->child(0)->pdgId();
	    VNeutrino.SetX(p->child(0)->px());
            VNeutrino.SetY(p->child(0)->py());
            eventInfo->auxdecor< float >("NeutrinoPt") = p->child(0)->pt();
            eventInfo->auxdecor< float >("NeutrinoEta") = p->child(0)->eta();
            eventInfo->auxdecor< float >("NeutrinoPhi") = p->child(0)->phi();
            eventInfo->auxdecor< float >("NeutrinoMass") = p->child(0)->m();
            realV=true;
          }
          else if (abs(p->child(1)->pdgId()) == 12 || abs(p->child(1)->pdgId()) == 14 || abs(p->child(1)->pdgId()) == 16){
            eventInfo->auxdecor< int >("NeutrinoId") = p->child(1)->pdgId();
            VNeutrino.SetX(p->child(1)->px());
            VNeutrino.SetY(p->child(1)->py());
            eventInfo->auxdecor< float >("NeutrinoPt") = p->child(1)->pt();
            eventInfo->auxdecor< float >("NeutrinoEta") = p->child(1)->eta();
            eventInfo->auxdecor< float >("NeutrinoPhi") = p->child(1)->phi();
            eventInfo->auxdecor< float >("NeutrinoMass") = p->child(1)->m();
            realV=true;
          }
	}
	if (abs(p->pdgId()) == 35){
	  eventInfo->auxdecor< float >("HPt") = p->pt();
	  eventInfo->auxdecor< float >("HEta") = p->eta();
	  eventInfo->auxdecor< float >("HPhi") = p->phi();
	  eventInfo->auxdecor< float >("HMass") = p->m();
	}
      }
      if (p->prodVtx() == nullptr || p->decayVtx() == nullptr){continue;}
      const xAOD::TruthParticle* child1;
      const xAOD::TruthParticle* child2;
      if (abs(p->pdgId()) == 36 && i==0){
	tbeta = p->p4().Beta();
	tgamma = p->p4().Gamma();
	auto vd = p->decayVtx()->v4();
	auto vp = p->prodVtx()->v4();
	tR_coord = vd.Perp();
	tX_coord = vd.X();
	tY_coord = vd.Y();
	tZ_coord = vd.Z();
	tlifetime_dil = (vd - vp).T();
	tlifetime_prop = (vd - vp).T()/tgamma;
	eventInfo->auxdecor< float >("beta1")= tbeta; 
	eventInfo->auxdecor< float >("gamma1")= tgamma;
	eventInfo->auxdecor< float >("R_coord1")= tR_coord;
	eventInfo->auxdecor< float >("X_coord1")= tX_coord;
	eventInfo->auxdecor< float >("Y_coord1")= tY_coord;
	eventInfo->auxdecor< float >("Z_coord1")= tZ_coord;
	eventInfo->auxdecor< float >("lifetime_dil1")= tlifetime_dil;
	eventInfo->auxdecor< float >("lifetime_prop1")= tlifetime_prop;
	eventInfo->auxdecor< float >("LLP_Pt1") = p->pt();
	eventInfo->auxdecor< float >("LLP_Eta1") = p->eta();
	eventInfo->auxdecor< float >("LLP_Phi1") = p->phi();
	eventInfo->auxdecor< float >("LLP_Mass1") = p->m();
	if (abs(p->child(0)->pdgId())==5){child1=Hto4bLLPFunctions::getBDecayChain(p->child(0)).back();}
	else {continue;}
	if (abs(p->child(1)->pdgId())==5){child2=Hto4bLLPFunctions::getBDecayChain(p->child(1)).back();}
	else {continue;}
	eventInfo->auxdecor< int >("B_index1") = i;
	eventInfo->auxdecor< float >("B_pt1") = child1->pt();
	eventInfo->auxdecor< float >("B_eta1") = child1->eta();
	eventInfo->auxdecor< float >("B_phi1") = child1->phi();
	eventInfo->auxdecor< float >("B_mass1") = child1->m();
	eventInfo->auxdecor< int >("B_index2") = i;
	eventInfo->auxdecor< float >("B_pt2") = child2->pt();
	eventInfo->auxdecor< float >("B_eta2") = child2->eta();
	eventInfo->auxdecor< float >("B_phi2") = child2->phi();
	eventInfo->auxdecor< float >("B_mass2") = child2->m();
	LLP_bs.push_back(child1);
	LLP_bs.push_back(child2);
	i++;
      }
      else if (abs(p->pdgId()) == 36 && i==1){
	tbeta = p->p4().Beta();
	tgamma = p->p4().Gamma();
	auto vd = p->decayVtx()->v4();
	auto vp = p->prodVtx()->v4();
	tR_coord = vd.Perp();
	tX_coord = vd.X();
	tY_coord = vd.Y();
	tZ_coord = vd.Z();
	tlifetime_dil = (vd - vp).T();
	tlifetime_prop = (vd - vp).T()/tgamma;
	eventInfo->auxdecor< float >("beta2")= tbeta;
	eventInfo->auxdecor< float >("gamma2")= tgamma;
	eventInfo->auxdecor< float >("R_coord2")= tR_coord;
	eventInfo->auxdecor< float >("X_coord2")= tX_coord;
	eventInfo->auxdecor< float >("Y_coord2")= tY_coord;
	eventInfo->auxdecor< float >("Z_coord2")= tZ_coord;
	eventInfo->auxdecor< float >("lifetime_dil2")= tlifetime_dil;
	eventInfo->auxdecor< float >("lifetime_prop2")= tlifetime_prop;
	eventInfo->auxdecor< float >("LLP_Pt2") = p->pt();
	eventInfo->auxdecor< float >("LLP_Eta2") = p->eta();
	eventInfo->auxdecor< float >("LLP_Phi2") = p->phi();
	eventInfo->auxdecor< float >("LLP_Mass2") = p->m();
        if (abs(p->child(0)->pdgId())==5){child1=Hto4bLLPFunctions::getBDecayChain(p->child(0)).back();}
        else {continue;}
        if (abs(p->child(1)->pdgId())==5){child2=Hto4bLLPFunctions::getBDecayChain(p->child(1)).back();}
        else {continue;}
	eventInfo->auxdecor< int >("B_index3") = i;
	eventInfo->auxdecor< float >("B_pt3") = child1->pt();
	eventInfo->auxdecor< float >("B_eta3") = child1->eta();
	eventInfo->auxdecor< float >("B_phi3") = child1->phi();
	eventInfo->auxdecor< float >("B_mass3") = child1->m();
	eventInfo->auxdecor< int >("B_index4") = i;
	eventInfo->auxdecor< float >("B_pt4") = child2->pt();
	eventInfo->auxdecor< float >("B_eta4") = child2->eta();
	eventInfo->auxdecor< float >("B_phi4") = child2->phi();
	eventInfo->auxdecor< float >("B_mass4") = child2->m();
	LLP_bs.push_back(child1);
	LLP_bs.push_back(child2);
      }
    }
  

  
  TVector2 VTrackMet = TVector2(0.0,0.0);
  for (auto track: *tracks){
    TVector2 VTrack = TVector2(0.0,0.0);
    VTrack.SetX(track->p4().Px());
    VTrack.SetY(track->p4().Py());
    VTrackMet = VTrackMet - VTrack;
  }
  VTrackMet = VTrackMet - VNeutrino;
  eventInfo->auxdecor< float >("TrackMet") = VTrackMet.Mod();

  
    if (LLP_bs.size() != 0){
      for( auto jet : *signalJets){
	int i=0;
	int nB=0;
	int LLPindex = -1;
	TLorentzVector VJet = TLorentzVector(0.0,0.0,0.0,0.0);
	VJet.SetPtEtaPhiE(jet->pt(), jet->eta(), jet->phi(), jet->e());
	float Rmin=9999;
	for( auto p : LLP_bs) {
	  if (p == nullptr){continue;}
	  TLorentzVector Vchild=TLorentzVector(0.0,0.0,0.0,0.0);
	  Vchild.SetPtEtaPhiE(p->pt(),p->eta(),p->phi(),p->e());
	  if (VJet.DeltaR(Vchild)<Rmin){Rmin=VJet.DeltaR(Vchild);}
	  if (i<2){
	    if (VJet.DeltaR(Vchild)<0.2){
	      LLPindex=0;
	      nB++;
	    }
	    i++;
	  }
	  else {
	    if (VJet.DeltaR(Vchild)<0.2){
	      if (LLPindex==0){
		LLPindex=2;
		nB++;
	      }
	      else {
		LLPindex=1;
		nB++;
	      }
	    }
	  }
	}
      jet->auxdecor< int >("LLP_index")=LLPindex;
      jet->auxdecor< int >("numB")=nB;
      jet->auxdecor< float >("nearestB")=Rmin;
      }
    }
  

  

    if (LLP_bs.size() != 0){
      int i=0;
      for (auto b : LLP_bs){
	if (b == nullptr){continue;}
	int isMatched = 0;
	TLorentzVector Vchild=TLorentzVector(0.0,0.0,0.0,0.0);
	Vchild.SetPtEtaPhiE(b->pt(),b->eta(),b->phi(),b->e());
	for( auto jet : *signalJets){
	  TLorentzVector VJet = TLorentzVector(0.0,0.0,0.0,0.0);
	  VJet.SetPtEtaPhiE(jet->pt(), jet->eta(), jet->phi(), jet->e());
	  if (VJet.DeltaR(Vchild)<0.2){
	    isMatched = 1;
	  }
	}
	float min=9999;
	for (auto p : LLP_bs){
	  if (p == nullptr){continue;}
	  TLorentzVector Vchild2 = TLorentzVector(0.0,0.0,0.0,0.0);
	  Vchild2.SetPtEtaPhiE(p->pt(),p->eta(),p->phi(),p->e());
	  if (Vchild2.DeltaR(Vchild)>0 && Vchild2.DeltaR(Vchild)<min){
	    min=Vchild2.DeltaR(Vchild);
	  }
	}
	if (i==0){
	  eventInfo->auxdecor<int>("B_matched1")=isMatched;
	  eventInfo->auxdecor<float>("B_nearest1")=min;
	}
	else if (i==1){
	  eventInfo->auxdecor<int>("B_matched2")=isMatched;
	  eventInfo->auxdecor<float>("B_nearest2")=min;
	}
	else if (i==2){
	  eventInfo->auxdecor<int>("B_matched3")=isMatched;
	  eventInfo->auxdecor<float>("B_nearest3")=min;
	}
	else if (i==3){
	  eventInfo->auxdecor<int>("B_matched4")=isMatched;
	  eventInfo->auxdecor<float>("B_nearest4")=min;
	}
	i++;
      }
    }
  }



  //End Amber's Addition#############################################################################################################################################


  ANA_MSG_DEBUG("Event # "<< m_eventCounter);

  /////////////////////////////////////// Output Plots ////////////////////////////////
  //float weight(1);
  //if( eventInfo->isAvailable< float >( "weight" ) ) {
  //  weight = eventInfo->auxdecor< float >( "weight" );
  //}

  return true;
}


//This grabs cross section, acceptance, and eventNumber information from the respective text file
//text format:     147915 2.3793E-01 5.0449E-03 499000
EL::StatusCode Hto4bLLPAlgorithm::getLumiWeights(const xAOD::EventInfo* eventInfo) {

  if(!m_isMC){
    m_mcChannelNumber = eventInfo->runNumber();
    m_xs = 1;
    m_filtEff = 1;
    m_numAMIEvents = 0;
    return EL::StatusCode::SUCCESS;
  }

  cout<<"Running getLumiWeights"<<endl;

  m_mcChannelNumber = eventInfo->mcChannelNumber();
  //if mcChannelNumber = 0 need to retrieve from runNumber
  if(eventInfo->mcChannelNumber()==0) m_mcChannelNumber = eventInfo->runNumber();
  cout<<"Channel Number Is: "<<m_mcChannelNumber<<endl;
  ifstream fileIn(  gSystem->ExpandPathName( ("$TestArea/Hto4bLLPAlgorithm/data/XsAcc_"+m_comEnergy+".txt").c_str() ) );
  cout<<"accessed Test Area"<<endl;
  std::string runNumStr = std::to_string( m_mcChannelNumber );
  std::string line;
  std::string subStr;
  while (getline(fileIn, line)){
    istringstream iss(line);
    iss >> subStr;
    if (subStr.find(runNumStr) != string::npos){
      iss >> subStr;
      sscanf(subStr.c_str(), "%e", &m_xs);
      iss >> subStr;
      sscanf(subStr.c_str(), "%e", &m_filtEff);
      iss >> subStr;
      sscanf(subStr.c_str(), "%i", &m_numAMIEvents);
      cout << "Setting xs / acceptance / numAMIEvents to " << m_xs << ":" << m_filtEff << ":" << m_numAMIEvents << endl;
      continue;
    }
  }
  if( m_numAMIEvents == 0){
    cerr << "ERROR: Could not find proper file information for file number " << runNumStr << endl;
    return EL::StatusCode::FAILURE;
  }
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPAlgorithm :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPAlgorithm :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPAlgorithm :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  return EL::StatusCode::SUCCESS;
}
