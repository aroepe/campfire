#include <Hto4bLLPAlgorithm/Hto4bLLPAlgorithm.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class Hto4bLLPAlgorithm+;
#pragma link C++ class Hto4bLLPFilter+;
#pragma link C++ class Hto4bLLPNtuple+;
#endif
