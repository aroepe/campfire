#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"
#include "AthContainers/ConstDataVector.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include <Hto4bLLPAlgorithm/Hto4bLLPNtuple.h>
#include <Hto4bLLPAlgorithm/Hto4bLLPFunctions.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include <xAODTruth/TruthVertex.h>
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "TFile.h"
#include "TEnv.h"
#include "TSystem.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static float GeV = 1000.;


// this is needed to distribute the algorithm to the workers
ClassImp(Hto4bLLPNtuple)

Hto4bLLPNtuple :: Hto4bLLPNtuple () :
  m_cutflowHist(0),
  m_cutflowHistW(0),
  m_treeStream("tree")
{
// Here you put any code for the base initialization of variables,
// e.g. initialize all pointers to 0.  Note that you should only put
// the most basic initialization here, since this method will be
// called on both the submission and the worker node.  Most of your
// initialization code will go into histInitialize() and
// initialize().

  ANA_MSG_INFO("Hto4bLLPNtuple() : Calling constructor");

  m_inJetContainerName       = "";
  m_inputAlgo                = "";
  m_allJetContainerName      = "";
  m_allJetInputAlgo          = "";
  m_inMETContainerName       = "";
  m_inMETTrkContainerName    = "";
  m_msgLevel                 = MSG::INFO;
  m_useCutFlow               = true;
  m_writeTree                = true;
  m_MCPileupCheckContainer   = "AntiKt4TruthJets";
  m_truthLevelOnly           = false;
  m_eventDetailStr           = "truth pileup";
  m_trigDetailStr            = "";
  m_jetDetailStr             = "kinematic clean energy truth flavorTag";
  m_jetDetailStrSyst         = "kinematic clean energy";
  m_elDetailStr              = "kinematic clean energy truth flavorTag";
  m_muDetailStr              = "kinematic clean energy truth flavorTag";
  m_metDetailStr             = "metClus sigClus";
  m_metTrkDetailStr          = "metTrk sigTrk";
  m_trackDetailStr           = "";
}



EL::StatusCode  Hto4bLLPNtuple :: configure ()
{
  ANA_MSG_INFO("configure() : Configuring Hto4bLLPNtuple Interface.");

  if( m_MCPileupCheckContainer == "None" ) {
    m_useMCPileupCheck = false;
  }

  m_comEnergy = "13TeV";

  ANA_MSG_INFO("configure() : Hto4bLLPNtuple Interface succesfully configured! \n");

  if( m_inJetContainerName.empty() ) {
    ANA_MSG_ERROR("configure() : InputContainer string is empty!");
    return EL::StatusCode::FAILURE;
  }

  if( !m_truthLevelOnly &&  m_allJetContainerName.empty() ) {
    ANA_MSG_ERROR("configure() : AllJetInputContainer string is empty!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode Hto4bLLPNtuple :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  xAOD::Init( "Hto4bLLPNtuple" ).ignore(); // call before opening first file

  EL::OutputStream outForTree( m_treeStream );
  job.outputAdd (outForTree);
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPNtuple :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  ANA_MSG_INFO("histInitialize() : Calling histInitialize \n");

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Hto4bLLPNtuple :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every                                                                                                             
  // single file, e.g. collect a list of all lumi-blocks processed                                                                                                                   
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPNtuple :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,                                                                                                               
  // e.g. resetting branch addresses on trees.  If you are using                                                                                                                     
  // D3PDReader or a similar service this method is not needed.                                                                                                                      
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Hto4bLLPNtuple :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_MSG_DEBUG("initialize");

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  m_eventCounter = -1;

  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK (HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  if ( this->configure() == EL::StatusCode::FAILURE ) {
    ANA_MSG_ERROR("initialize() : Failed to properly configure. Exiting." );
    return EL::StatusCode::FAILURE;
  }

  // m_truthLevelOnly is set in config so need to do this after configure is called
  if( m_truthLevelOnly ) { m_isMC = true; }
  else {
    m_isMC = ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) ? true : false;
  }

  getLumiWeights(eventInfo);

  if(m_useCutFlow) {

    TFile *file = wk()->getOutputFile ("cutflow");
    m_cutflowHist  = (TH1D*)file->Get("cutflow");
    m_cutflowHistW = (TH1D*)file->Get("cutflow_weighted");

    m_cutflowFirst = m_cutflowHist->GetXaxis()->FindBin("TriggerEfficiency");
    m_cutflowHistW->GetXaxis()->FindBin("TriggerEfficiency");

    if(m_useMCPileupCheck && m_isMC){
      m_cutflowHist->GetXaxis()->FindBin("mcCleaning");
      m_cutflowHistW->GetXaxis()->FindBin("mcCleaning");
    }

    m_cutflowHist->GetXaxis()->FindBin("y*");
    m_cutflowHistW->GetXaxis()->FindBin("y*");

  }
  ANA_MSG_INFO("initialize() : Successfully initialized! \n");
  return EL::StatusCode::SUCCESS;
}

void Hto4bLLPNtuple::AddTree( std::string name ) {

  ANA_MSG_DEBUG("AddTree");

  std::string treeName("outTree");
  // naming convention
  treeName += name; // add systematic
  TTree * outTree = new TTree(treeName.c_str(),treeName.c_str());
  if( !outTree ) {
    ANA_MSG_ERROR("AddTree() : Failed to get output tree!");
    // FIXME!! kill here
  }
  TFile* treeFile = wk()->getOutputFile( m_treeStream );
  outTree->SetDirectory( treeFile );

  MiniTree* miniTree = new MiniTree(m_event, outTree, treeFile, m_store); //!!j
  // only limited information available in truth xAODs
  if( m_truthLevelOnly ) {
    miniTree->AddEvent("truth");
    miniTree->AddJets("kinematic");
  } else { // reconstructed xAOD
    miniTree->AddEvent( m_eventDetailStr );
    miniTree->AddTrigger( m_trigDetailStr );
    miniTree->AddMET( m_metDetailStr );
    miniTree->AddMET( m_metTrkDetailStr );
    miniTree->AddTrackParts ( "track" ,m_trackDetailStr );
    if( !name.empty() ) { // save limited information for systematic variations
      miniTree->AddJets( m_jetDetailStrSyst );
    } else {
      miniTree->AddJets( m_jetDetailStr );
      miniTree->AddMuons(m_muDetailStr);
      miniTree->AddElectrons(m_elDetailStr);
    }
  }
  m_myTrees[name] = miniTree;
  // see Worker.cxx line 134: the following function call takes ownership of the tree
  // from the treeFile; no output is written. so don't do that!
  // wk()->addOutput( outTree );

}

void Hto4bLLPNtuple::AddHists( std::string name ) {

  std::string jhName("highPtJets");
  //if( ! name.empty() ) { jhName += "."; } // makes it hard to do command line stuff
  jhName += name; // add systematic

}


EL::StatusCode Hto4bLLPNtuple :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  //cout<<"Hto4b execute"<<endl;

  ANA_MSG_DEBUG("execute(): Applying selection");
  ++m_eventCounter;

  m_iCutflow = m_cutflowFirst;

  //----------------------------
  // Event information
  //---------------------------

  ///////////////////////////// Retrieve Containers /////////////////////////////////////////

  ANA_MSG_DEBUG("execute() : Get Containers");
  const xAOD::EventInfo* eventInfo(nullptr);
  ANA_CHECK (HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store));

  if (m_eventCounter == 0 && m_isMC) {
    getLumiWeights(eventInfo);
  } else if (!m_isMC) {
    m_filtEff = m_xs = 1.0;
  }

  SG::AuxElement::ConstAccessor<float> NPVAccessor("NPV");
  const xAOD::VertexContainer* vertices = 0;
  if(!m_truthLevelOnly) {
    ANA_CHECK (HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store));
  }
  if(!m_truthLevelOnly && !NPVAccessor.isAvailable( *eventInfo )) { // NPV might already be available
    // number of PVs with 2 or more tracks
    //eventInfo->auxdecor< int >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
    // TMP for JetUncertainties uses the same variable
    eventInfo->auxdecor< float >( "NPV" ) = HelperFunctions::countPrimaryVertices(vertices, 2);
  }

  const xAOD::TrackParticleContainer* tracks = nullptr;
  ANA_CHECK (HelperFunctions::retrieve(tracks, "InDetTrackParticles", m_event, m_store));

  const xAOD::TruthParticleContainer* TruthPart = nullptr;
  if (m_isMC){ANA_CHECK (HelperFunctions::retrieve(TruthPart, "TruthParticles", m_event, m_store));}
 
  const xAOD::MissingETContainer* Met(0);
  ANA_CHECK (HelperFunctions::retrieve(Met, m_inMETContainerName, m_event, m_store));

  const xAOD::MissingETContainer* MetTrk(0);
  ANA_CHECK (HelperFunctions::retrieve(MetTrk, m_inMETTrkContainerName, m_event, m_store));

  //#####################################################################################################################################################
  const xAOD::JetContainer* truthJets = 0;
  if(m_useMCPileupCheck && m_isMC){
    ANA_CHECK (HelperFunctions::retrieve(truthJets, m_MCPileupCheckContainer, m_event, m_store));
  }

  //Set this first, as m_mcEventWeight is needed by passCut()
  if(m_isMC)
    m_mcEventWeight = eventInfo->mcEventWeight();
  else
    m_mcEventWeight = 1;

  const xAOD::MuonContainer* allMuons = 0;
  const xAOD::ElectronContainer* allElectrons = 0;
  ANA_CHECK (HelperFunctions::retrieve(allMuons, m_inMuContainerName, m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(allElectrons, m_inElContainerName, m_event, m_store));


  //%%%%%%%%%%%%%%%%%%%%%%%%%%%% Loop over Systematics %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  ANA_MSG_DEBUG("execute() : Systematic Loop");

  // did any collection pass the cuts?
  bool pass(false);
  bool doCutflow(m_useCutFlow); // will only stay true for nominal
  const xAOD::JetContainer* signalJets = 0;
  const xAOD::JetContainer* allJets = 0;

  // if input comes from xAOD, or just running one collection,
  // then get the one collection and be done with it
  if( m_inputAlgo == "" || m_truthLevelOnly ) {
    ANA_CHECK (HelperFunctions::retrieve(signalJets, m_inJetContainerName, m_event, m_store));
    if(!m_truthLevelOnly) { ANA_CHECK (HelperFunctions::retrieve(allJets, m_allJetContainerName, m_event, m_store)); }

    // executeAnalysis
    pass = this->fillTree( eventInfo, signalJets, truthJets, allJets, allMuons, allElectrons, vertices, tracks, TruthPart, Met, MetTrk, doCutflow, "" );

  }
  else { // get the list of systematics to run over

    // get vector of string giving the names
    std::vector<std::string>* systNames = 0;
    if ( m_store->contains< std::vector<std::string> >( m_inputAlgo ) ) {
      if(!m_store->retrieve( systNames, m_inputAlgo ).isSuccess()) {
        ANA_MSG_INFO("execute() : Cannot find vector from "<<m_inputAlgo.c_str());
        return StatusCode::FAILURE;
      }
    }


    // loop over systematics
    bool saveContainerNames(false);
    std::vector< std::string >* vecOutContainerNames = 0;
    if(saveContainerNames) { vecOutContainerNames = new std::vector< std::string >; }
    // should only doCutflow for the nominal
    bool passOne(false);
    std::string inContainerName("");
    std::string allJetContainerName("");
    if (systNames->size() != 0){
      for( auto systName : *systNames ) {
	ANA_MSG_DEBUG("execute() : Systematic Loop"<<systName);
	inContainerName = m_inJetContainerName+systName;
	ANA_CHECK (HelperFunctions::retrieve(signalJets, inContainerName, m_event, m_store));
	//
	allJetContainerName = m_allJetContainerName+systName;
	ANA_CHECK (HelperFunctions::retrieve(allJets, allJetContainerName, m_event, m_store));
	//
	// allign with Dijet naming conventions
	if( systName.empty() ) { doCutflow = m_useCutFlow; } // only doCutflow for nominal
	else { doCutflow = false; }
	passOne = this->fillTree( eventInfo, signalJets, truthJets, allJets, allMuons, allElectrons, vertices, tracks, TruthPart, Met, MetTrk, doCutflow, systName );
	// save the string if passing the selection
	if( saveContainerNames && passOne ) { vecOutContainerNames->push_back( systName ); }
	// the final decision - if at least one passes keep going!
	pass = pass || passOne;

      }
    }
    // save list of systs that should be considered down stream
    if( saveContainerNames ) {
      ANA_CHECK (m_store->record( vecOutContainerNames, m_name));
    }
  }

  //if(!pass) {
  //wk()->skipEvent();
  //}
  return EL::StatusCode::SUCCESS;

}



bool Hto4bLLPNtuple :: fillTree ( const xAOD::EventInfo* eventInfo,
    const xAOD::JetContainer* signalJets,
    const xAOD::JetContainer* truthJets,
    const xAOD::JetContainer* allJets,
    const xAOD::MuonContainer* allMuons,
    const xAOD::ElectronContainer* allElectrons,
    const xAOD::VertexContainer* vertices,
    const xAOD::TrackParticleContainer* tracks,
    const xAOD::TruthParticleContainer* TruthPart,
    const xAOD::MissingETContainer* Met,
    const xAOD::MissingETContainer* MetTrk,
    bool doCutflow,
    std::string systName) {

  /////////////////////////// Begin Selections  ///////////////////////////////

  int evtNum = eventInfo->eventNumber();

  if(doCutflow) Hto4bLLPFunctions::passCut(m_cutflowHist, m_cutflowHistW, m_iCutflow, m_mcEventWeight); //TriggerEfficiency

  ANA_MSG_DEBUG("Event # "<< m_eventCounter);

  const xAOD::JetContainer* good_jets;
  const xAOD::JetContainer* selected_jets;
  const xAOD::JetContainer* signal_jets;
  ANA_CHECK (HelperFunctions::retrieve(good_jets, "good_jets", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_jets, "selected_jets", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(signal_jets, "signal_jets", m_event, m_store));

  const xAOD::ElectronContainer* good_electrons;
  const xAOD::ElectronContainer* selected_electrons;
  ANA_CHECK (HelperFunctions::retrieve(good_electrons, "good_electrons", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_electrons, "selected_electrons", m_event, m_store));

  const xAOD::MuonContainer* good_muons;
  const xAOD::MuonContainer* selected_muons;
  ANA_CHECK (HelperFunctions::retrieve(good_muons, "good_muons", m_event, m_store));
  ANA_CHECK (HelperFunctions::retrieve(selected_muons, "selected_muons", m_event, m_store));

  if(m_writeTree){
    if (m_myTrees.find( systName ) == m_myTrees.end() ) { AddTree(systName); }
    if(eventInfo) { m_myTrees[systName]->FillEvent(eventInfo, m_event); }
    if(Met) { m_myTrees[systName]->FillMET(Met); }
    if(MetTrk) {m_myTrees[systName]->FillMET(MetTrk); }
    if(tracks) {m_myTrees[systName]->FillTracks("track", tracks);}
    if( m_truthLevelOnly ) {
      if(allJets)  { m_myTrees[systName]->FillJets( allJets, -1 ); }
    } else {
      m_myTrees[systName]->FillTrigger( eventInfo );
      if(signal_jets)  m_myTrees[systName]->FillJets(signal_jets, HelperFunctions::getPrimaryVertexLocation( vertices )  );
      if(selected_muons) m_myTrees[systName]->FillMuons(selected_muons,HelperFunctions::getPrimaryVertex(vertices));
      if(selected_electrons) m_myTrees[systName]->FillElectrons(selected_electrons,HelperFunctions::getPrimaryVertex(vertices));
    }
    m_myTrees[systName]->Fill();
  }

  ANA_MSG_DEBUG("Tree Written");

  return true;
}


//This grabs cross section, acceptance, and eventNumber information from the respective text file
//text format:     147915 2.3793E-01 5.0449E-03 499000
EL::StatusCode Hto4bLLPNtuple::getLumiWeights(const xAOD::EventInfo* eventInfo) {

  if(!m_isMC){
    m_mcChannelNumber = eventInfo->runNumber();
    m_xs = 1;
    m_filtEff = 1;
    m_numAMIEvents = 0;
    return EL::StatusCode::SUCCESS;
  }

  cout<<"Running getLumiWeights"<<endl;

  m_mcChannelNumber = eventInfo->mcChannelNumber();
  //if mcChannelNumber = 0 need to retrieve from runNumber
  if(eventInfo->mcChannelNumber()==0) m_mcChannelNumber = eventInfo->runNumber();
  cout<<"Channel Number Is: "<<m_mcChannelNumber<<endl;
  ifstream fileIn(  gSystem->ExpandPathName( ("$TestArea/Hto4bLLPNtuple/data/XsAcc_"+m_comEnergy+".txt").c_str() ) );
  cout<<"accessed Test Area"<<endl;
  std::string runNumStr = std::to_string( m_mcChannelNumber );
  std::string line;
  std::string subStr;
  while (getline(fileIn, line)){
    istringstream iss(line);
    iss >> subStr;
    if (subStr.find(runNumStr) != string::npos){
      iss >> subStr;
      sscanf(subStr.c_str(), "%e", &m_xs);
      iss >> subStr;
      sscanf(subStr.c_str(), "%e", &m_filtEff);
      iss >> subStr;
      sscanf(subStr.c_str(), "%i", &m_numAMIEvents);
      cout << "Setting xs / acceptance / numAMIEvents to " << m_xs << ":" << m_filtEff << ":" << m_numAMIEvents << endl;
      continue;
    }
  }
  if( m_numAMIEvents == 0){
    cerr << "ERROR: Could not find proper file information for file number " << runNumStr << endl;
    return EL::StatusCode::FAILURE;
  }
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode Hto4bLLPNtuple :: postExecute ()
{
  // Here you do everything that needs to be done after the main event                                                                                                               
  // processing.  This is typically very rare, particularly in user                                                                                                                  
  // code.  It is mainly used in implementing the NTupleSvc.                                                                                                                         
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode Hto4bLLPNtuple :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Hto4bLLPNtuple :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  if( m_writeTree ) {
    std::string thisName;
    //m_ss.str( std::string() );
    //m_ss << m_mcChannelNumber;
    TFile * treeFile = wk()->getOutputFile( m_treeStream );
    if(m_useCutFlow) {
      TH1F* thisCutflowHist = (TH1F*) m_cutflowHist->Clone();
      thisName = thisCutflowHist->GetName();
      thisCutflowHist->SetName( (thisName).c_str() );
      //thisCutflowHist->SetName( (thisName+"_"+m_ss.str()).c_str() );
      thisCutflowHist->SetDirectory( treeFile );

      TH1F* thisCutflowHistW = (TH1F*) m_cutflowHistW->Clone();
      thisName = thisCutflowHistW->GetName();
      thisCutflowHistW->SetName( (thisName).c_str() );
      //thisCutflowHistW->SetName( (thisName+"_"+m_ss.str()).c_str() );
      thisCutflowHistW->SetDirectory( treeFile );
    }
    // Get MetaData_EventCount histogram
    TFile* metaDataFile = wk()->getOutputFile( "metadata" );
    TH1D* metaDataHist = (TH1D*) metaDataFile->Get("MetaData_EventCount");
    TH1D* thisMetaDataHist = (TH1D*) metaDataHist->Clone();
    thisMetaDataHist->SetDirectory( treeFile );
  }

  return EL::StatusCode::SUCCESS;
}
