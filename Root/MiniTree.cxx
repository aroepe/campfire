#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODAnaHelpers/HelperFunctions.h"

#include "Hto4bLLPAlgorithm/MiniTree.h"

MiniTree :: MiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, xAOD::TStore* store /* = 0 */) :
  HelpTreeBase(event, tree, file, 1e3)
{
  Info("MiniTree", "Creating output TTree");
  tree->SetAutoSave(1000000000);
  m_firstEvent = true;
  m_store = store;

}

MiniTree :: ~MiniTree()
{
}
//////////////////// Connect Defined variables to branches here /////////////////////////////
void MiniTree::AddEventUser(const std::string detailStr)
{
  // event variables
  m_tree->Branch("mjj",  &m_mjj,  "mjj/F");
  m_tree->Branch("m3j", &m_m3j, "m3j/F");
  m_tree->Branch("mnj", &m_mnj);
  m_tree->Branch("LLP_beta", &m_LLP_beta);
  m_tree->Branch("LLP_gamma", &m_LLP_gamma);
  m_tree->Branch("LLP_R_coord", &m_LLP_R_coord);
  m_tree->Branch("LLP_X_coord", &m_LLP_X_coord);
  m_tree->Branch("LLP_Y_coord", &m_LLP_Y_coord);
  m_tree->Branch("LLP_Z_coord", &m_LLP_Z_coord);
  m_tree->Branch("LLP_lifetime_dil", &m_LLP_lifetime_dil);
  m_tree->Branch("LLP_lifetime_prop", &m_LLP_lifetime_prop);
  m_tree->Branch("V_Child", &m_Vchild);
  m_tree->Branch("V_Child_Pt", &m_VchildPt);
  m_tree->Branch("V_Id",&m_VId);
  m_tree->Branch("V_Pt",&m_VPt);
  m_tree->Branch("V_Eta",&m_VEta);
  m_tree->Branch("V_Phi",&m_VPhi);
  m_tree->Branch("V_Mass",&m_VMass);
  m_tree->Branch("Neutrino_Id",&m_NeutrinoId);
  m_tree->Branch("Neutrino_Pt",&m_NeutrinoPt);
  m_tree->Branch("Neutrino_Eta",&m_NeutrinoEta);
  m_tree->Branch("Neutrino_Phi",&m_NeutrinoPhi);
  m_tree->Branch("Neutrino_Mass",&m_NeutrinoMass);
  m_tree->Branch("Track_Met", &m_TrackMet);
  m_tree->Branch("H_Pt",&m_HPt);
  m_tree->Branch("H_Eta",&m_HEta);
  m_tree->Branch("H_Phi",&m_HPhi);
  m_tree->Branch("H_Mass",&m_HMass);
  m_tree->Branch("LLP_Pt",&m_LLP_Pt);
  m_tree->Branch("LLP_Eta",&m_LLP_Eta);
  m_tree->Branch("LLP_Phi",&m_LLP_Phi);
  m_tree->Branch("LLP_Mass",&m_LLP_Mass);
  m_tree->Branch("V_Child", &m_Vchild);
  m_tree->Branch("B_Index",&m_BIndex);
  m_tree->Branch("B_Pt",&m_BPt);
  m_tree->Branch("B_Eta",&m_BEta);
  m_tree->Branch("B_Phi",&m_BPhi);
  m_tree->Branch("B_Mass",&m_BMass);
  m_tree->Branch("B_Matched", &m_BMatched);
  m_tree->Branch("B_Nearest", &m_BNearest);
  m_tree->Branch("passesFilter",&m_passesFilter);
  m_tree->Branch("passesElecFilter", &m_passesElecFilter);
  m_tree->Branch("passesMuonFilter", &m_passesMuonFilter);
  m_tree->Branch("Met", &m_Met);
  m_tree->Branch("Met_Phi", &m_Met_Phi);
  m_tree->Branch("Met_Sumet", &m_Met_Sumet);
  m_tree->Branch("Lep_JetMinCHF_DeltaPhi", &m_Lep_JetMinCHF_DeltaPhi);
  m_tree->Branch("Lep_SumLeadingJets_DeltaPhi", &m_Lep_SumLeadingJets_DeltaPhi);
  m_tree->Branch("Vec_JetMinCHF_DeltaPhi", &m_Vec_JetMinCHF_DeltaPhi);
  m_tree->Branch("Vec_SumLeadingJets_DeltaPhi", &m_Vec_SumLeadingJets_DeltaPhi);
  m_tree->Branch("MetDeltaPhi", &m_metDeltaPhi);

  // weights
  m_tree->Branch("weight", &m_weight, "weight/F");
  m_tree->Branch("weight_xs", &m_weight_xs, "weight_xs/F");
  m_tree->Branch("weight_prescale", &m_weight_prescale, "weight_prescale/F");
  m_tree->Branch("weight_resonanceKFactor", &m_weight_resonanceKFactor, "weight_resonanceKFactor/F");

}

void MiniTree::AddJetsUser(const std::string detailStr, const std::string jetName)
{
  m_tree->Branch("jet_alpha_max", &m_jet_alpha_max);
  m_tree->Branch("jet_alpha_max_pass", &m_jet_alpha_max_pass);
  m_tree->Branch("jet_chf", &m_jet_chf);
  m_tree->Branch("jet_chf_pass", &m_jet_chf_pass);
  m_tree->Branch("jet_track_pt_max", &m_jet_track_pt_max);
  m_tree->Branch("jet_ntracks", &m_jet_ntracks);
  m_tree->Branch("jet_LooseDR", &m_jet_LooseDR);
  m_tree->Branch("jet_MediumDR",&m_jet_MediumDR);
  m_tree->Branch("jet_TightDR",&m_jet_TightDR);
  m_tree->Branch("jet_LooseDPhi", &m_jet_LooseDPhi);
  m_tree->Branch("jet_MediumDPhi",&m_jet_MediumDPhi);
  m_tree->Branch("jet_TightDPhi",&m_jet_TightDPhi);
  m_tree->Branch("jet_LLP_index", &m_jet_LLP_index);
  m_tree->Branch("jet_num_b", &m_jet_num_b);
  m_tree->Branch("jet_nearest_b", &m_jet_nearest_b);
}

void MiniTree::AddElectronsUser(const std::string detailStr)
{
  m_tree->Branch("el_isGood", &m_el_isGood);
}

void MiniTree::AddTracksUser( const std::string trackName, const std::string detailStr)
{
  m_tree->Branch("track_isAssoc", &m_track_isAssoc);
  m_tree->Branch("track_jetIndex", &m_track_jetIndex);
}

//////////////////// Clear any defined vectors here ////////////////////////////
void MiniTree::ClearEventUser() {
  m_mjj      = -999;
  m_m3j      = -999;
  m_mnj      = -999;
  m_LLP_beta.clear();
  m_LLP_gamma.clear();
  m_LLP_R_coord.clear();
  m_LLP_X_coord.clear();
  m_LLP_Y_coord.clear();
  m_LLP_Z_coord.clear();
  m_LLP_lifetime_dil.clear();
  m_LLP_lifetime_prop.clear();
  m_Vchild   = -999;
  m_VchildPt = -999;
  m_VId      = -999;
  m_VPt      = -999;
  m_VEta     = -999;
  m_VPhi     = -999;
  m_VMass    = -999;
  m_NeutrinoId      = -999;
  m_NeutrinoPt      = -999;
  m_NeutrinoEta     = -999;
  m_NeutrinoPhi     = -999;
  m_NeutrinoMass    = -999;
  m_TrackMet = -999;
  m_HPt      = -999;
  m_HEta     = -999;
  m_HPhi     = -999;
  m_HMass    = -999;
  m_BIndex.clear();
  m_BPt.clear();
  m_BEta.clear();
  m_BPhi.clear();
  m_BMatched.clear();
  m_BNearest.clear();
  m_LLP_Pt.clear();
  m_LLP_Eta.clear();
  m_LLP_Phi.clear();
  m_LLP_Mass.clear();
  m_passesFilter = -999;
  m_passesElecFilter = -999;
  m_passesMuonFilter = -999;
  m_Met = -999;
  m_Met_Phi = -999;
  m_Met_Sumet = -999;
  m_Lep_JetMinCHF_DeltaPhi = -999;
  m_Lep_SumLeadingJets_DeltaPhi = -999;
  m_Vec_JetMinCHF_DeltaPhi = -999;
  m_Vec_SumLeadingJets_DeltaPhi = -999;
  m_metDeltaPhi = -999;

  m_weight_corr = -999;
  m_weight    = -999;
  m_weight_xs = -999;
  m_weight_prescale = -999;
  m_weight_resonanceKFactor = -999;
}

void MiniTree::ClearJetsUser(const std::string jetName ) {
  m_jet_alpha_max.clear();
  m_jet_alpha_max_pass.clear();
  m_jet_chf.clear();
  m_jet_chf_pass.clear();
  m_jet_track_pt_max.clear();
  m_jet_ntracks.clear();
  m_jet_LooseDR.clear();
  m_jet_MediumDR.clear();
  m_jet_TightDR.clear();
  m_jet_LooseDPhi.clear();
  m_jet_MediumDPhi.clear();
  m_jet_TightDPhi.clear();
  m_jet_LLP_index.clear();
  m_jet_num_b.clear();
  m_jet_nearest_b.clear();
}

void MiniTree::ClearElectronsUser(const std::string elecName ) {
  m_el_isGood.clear();
}

void MiniTree::ClearTracksUser(const std::string trackName ) {
  m_track_isAssoc.clear();
  m_track_jetIndex.clear();
}

/////////////////// Assign values to defined event variables here ////////////////////////
void MiniTree::FillEventUser( const xAOD::EventInfo* eventInfo ) {

  if( eventInfo->isAvailable< float >( "mjj" ) )
    m_mjj = eventInfo->auxdecor< float >( "mjj" );
  if( eventInfo->isAvailable< float >( "m3j" ) )
    m_m3j = eventInfo->auxdecor< float >( "m3j" );
  if( eventInfo->isAvailable< float >( "mnj" ) )
    m_mnj = eventInfo->auxdecor< float >( "mnj" );

  if( eventInfo->isAvailable< float >( "beta1" ) ){
    m_LLP_beta.push_back(eventInfo->auxdecor< float >( "beta1" ));}

  if( eventInfo->isAvailable< float >( "gamma1" ) ){
    m_LLP_gamma.push_back(eventInfo->auxdecor< float >( "gamma1" ));}

  if( eventInfo->isAvailable< float >( "R_coord1" ) ){
    m_LLP_R_coord.push_back(eventInfo->auxdecor< float >( "R_coord1" ));}

  if( eventInfo->isAvailable< float >( "X_coord1" ) ){
    m_LLP_X_coord.push_back(eventInfo->auxdecor< float >( "X_coord1" ));}

  if( eventInfo->isAvailable< float >( "Y_coord1" ) ){
    m_LLP_Y_coord.push_back(eventInfo->auxdecor< float >( "Y_coord1" ));}

  if( eventInfo->isAvailable< float >( "Z_coord1" ) ){
    m_LLP_Z_coord.push_back(eventInfo->auxdecor< float >( "Z_coord1" ));}

  if( eventInfo->isAvailable< float >( "lifetime_dil1" ) ){
    m_LLP_lifetime_dil.push_back(eventInfo->auxdecor< float >( "lifetime_dil1" ));}

  if( eventInfo->isAvailable< float >( "lifetime_prop1" ) ){
    m_LLP_lifetime_prop.push_back(eventInfo->auxdecor< float >( "lifetime_prop1" ));}

  if( eventInfo->isAvailable< float >( "beta2" ) ){
    m_LLP_beta.push_back(eventInfo->auxdecor< float >( "beta2" ));}

  if( eventInfo->isAvailable< float >( "gamma2" ) ){
    m_LLP_gamma.push_back(eventInfo->auxdecor< float >( "gamma2" ));}

  if( eventInfo->isAvailable< float >( "R_coord2" ) ){
    m_LLP_R_coord.push_back(eventInfo->auxdecor< float >( "R_coord2" ));}

  if( eventInfo->isAvailable< float >( "X_coord2" ) ){
    m_LLP_X_coord.push_back(eventInfo->auxdecor< float >( "X_coord2" ));}

  if( eventInfo->isAvailable< float >( "Y_coord2" ) ){
    m_LLP_Y_coord.push_back(eventInfo->auxdecor< float >( "Y_coord2" ));}

  if( eventInfo->isAvailable< float >( "Z_coord2" ) ){
    m_LLP_Z_coord.push_back(eventInfo->auxdecor< float >( "Z_coord2" ));}

  if( eventInfo->isAvailable< float >( "lifetime_dil2" ) ){
    m_LLP_lifetime_dil.push_back(eventInfo->auxdecor< float >( "lifetime_dil2" ));}

  if( eventInfo->isAvailable< float >( "lifetime_prop2" ) ){
    m_LLP_lifetime_prop.push_back(eventInfo->auxdecor< float >( "lifetime_prop2" ));}

  if( eventInfo->isAvailable< float >( "LLP_Pt1" ) ){
    m_LLP_Pt.push_back(eventInfo->auxdecor< float >( "LLP_Pt1" ));}

  if( eventInfo->isAvailable< float >( "LLP_Eta1" ) ){
    m_LLP_Eta.push_back(eventInfo->auxdecor< float >( "LLP_Eta1" ));}

  if( eventInfo->isAvailable< float >( "LLP_Phi1" ) ){
    m_LLP_Phi.push_back(eventInfo->auxdecor< float >( "LLP_Phi1" ));}

  if( eventInfo->isAvailable< float >( "LLP_Mass1" ) ){
    m_LLP_Mass.push_back(eventInfo->auxdecor< float >( "LLP_Mass1" ));}

  if( eventInfo->isAvailable< float >( "LLP_Pt2" ) ){
    m_LLP_Pt.push_back(eventInfo->auxdecor< float >( "LLP_Pt2" ));}

  if( eventInfo->isAvailable< float >( "LLP_Eta2" ) ){
    m_LLP_Eta.push_back(eventInfo->auxdecor< float >( "LLP_Eta2" ));}

  if( eventInfo->isAvailable< float >( "LLP_Phi2" ) ){
    m_LLP_Phi.push_back(eventInfo->auxdecor< float >( "LLP_Phi2" ));}

  if( eventInfo->isAvailable< float >( "LLP_Mass2" ) ){
    m_LLP_Mass.push_back(eventInfo->auxdecor< float >( "LLP_Mass2" ));}


  if( eventInfo->isAvailable< int >( "Vchild" ) )
    m_Vchild = eventInfo->auxdecor< int >( "Vchild" );
  if( eventInfo->isAvailable< float >( "VchildPt" ) )
    m_VchildPt = eventInfo->auxdecor< float >( "VchildPt" );
  if( eventInfo->isAvailable< int >( "VId" ) )
    m_VId = eventInfo->auxdecor< int >( "VId" );
  if( eventInfo->isAvailable< float >( "VPt" ) )
    m_VPt = eventInfo->auxdecor< float >( "VPt" );
  if( eventInfo->isAvailable< float >( "VEta" ) )
    m_VEta = eventInfo->auxdecor< float >( "VEta" );
  if( eventInfo->isAvailable< float >( "VPhi" ) )
    m_VPhi = eventInfo->auxdecor< float >( "VPhi" );
  if( eventInfo->isAvailable< float >( "VMass" ) )
    m_VMass = eventInfo->auxdecor< float >( "VMass" );
  if( eventInfo->isAvailable< int >( "NeutrinoId" ) )
    m_NeutrinoId = eventInfo->auxdecor< int >( "NeutrinoId" );
  if( eventInfo->isAvailable< float >( "NeutrinoPt" ) )
    m_NeutrinoPt = eventInfo->auxdecor< float >( "NeutrinoPt" );
  if( eventInfo->isAvailable< float >( "NeutrinoEta" ) )
    m_NeutrinoEta = eventInfo->auxdecor< float >( "NeutrinoEta" );
  if( eventInfo->isAvailable< float >( "NeutrinoPhi" ) )
    m_NeutrinoPhi = eventInfo->auxdecor< float >( "NeutrinoPhi" );
  if( eventInfo->isAvailable< float >( "NeutrinoMass" ) )
    m_NeutrinoMass = eventInfo->auxdecor< float >( "NeutrinoMass" );
  if( eventInfo->isAvailable< float >( "TrackMet" ) )
    m_TrackMet = eventInfo->auxdecor< float >( "TrackMet" );
  if( eventInfo->isAvailable< float >( "HPt" ) )
    m_HPt = eventInfo->auxdecor< float >( "HPt" );
  if( eventInfo->isAvailable< float >( "HEta" ) )
    m_HEta = eventInfo->auxdecor< float >( "HEta" );
  if( eventInfo->isAvailable< float >( "HPhi" ) )
    m_HPhi = eventInfo->auxdecor< float >( "HPhi" );
  if( eventInfo->isAvailable< float >( "HMass" ) )
    m_HMass = eventInfo->auxdecor< float >( "HMass" );

  if( eventInfo->isAvailable< int >( "B_index1" ) )
    m_BIndex.push_back(eventInfo->auxdecor< int >( "B_index1" ));
  if( eventInfo->isAvailable< float >( "B_pt1" ) )
    m_BPt.push_back(eventInfo->auxdecor< float >( "B_pt1" ));
  if( eventInfo->isAvailable< float >( "B_eta1" ) )
    m_BEta.push_back(eventInfo->auxdecor< float >( "B_eta1" ));
  if( eventInfo->isAvailable< float >( "B_phi1" ) )
    m_BPhi.push_back(eventInfo->auxdecor< float >( "B_phi1" ));
  if( eventInfo->isAvailable< float >( "B_mass1" ) )
    m_BMass.push_back(eventInfo->auxdecor< float >( "B_mass1" ));
  if( eventInfo->isAvailable< int >( "B_matched1" ) )
    m_BMatched.push_back(eventInfo->auxdecor< int >( "B_matched1" ));
  if( eventInfo->isAvailable< float >( "B_nearest1" ) )
    m_BNearest.push_back(eventInfo->auxdecor< float >( "B_nearest1" ));

  if( eventInfo->isAvailable< int >( "B_index2" ) )
    m_BIndex.push_back(eventInfo->auxdecor< int >( "B_index2" ));
  if( eventInfo->isAvailable< float >( "B_pt2" ) )
    m_BPt.push_back(eventInfo->auxdecor< float >( "B_pt2" ));
  if( eventInfo->isAvailable< float >( "B_eta2" ) )
    m_BEta.push_back(eventInfo->auxdecor< float >( "B_eta2" ));
  if( eventInfo->isAvailable< float >( "B_phi2" ) )
    m_BPhi.push_back(eventInfo->auxdecor< float >( "B_phi2" ));
  if( eventInfo->isAvailable< float >( "B_mass2" ) )
    m_BMass.push_back(eventInfo->auxdecor< float >( "B_mass2" ));
  if( eventInfo->isAvailable< int >( "B_matched2" ) )
    m_BMatched.push_back(eventInfo->auxdecor< int >( "B_matched2" ));
  if( eventInfo->isAvailable< float >( "B_nearest2" ) )
    m_BNearest.push_back(eventInfo->auxdecor< float >( "B_nearest2" ));

  if( eventInfo->isAvailable< int >( "B_index3" ) )
    m_BIndex.push_back(eventInfo->auxdecor< int >( "B_index3" ));
  if( eventInfo->isAvailable< float >( "B_pt3" ) )
    m_BPt.push_back(eventInfo->auxdecor< float >( "B_pt3" ));
  if( eventInfo->isAvailable< float >( "B_eta3" ) )
    m_BEta.push_back(eventInfo->auxdecor< float >( "B_eta3" ));
  if( eventInfo->isAvailable< float >( "B_phi3" ) )
    m_BPhi.push_back(eventInfo->auxdecor< float >( "B_phi3" ));
  if( eventInfo->isAvailable< float >( "B_mass3" ) )
    m_BMass.push_back(eventInfo->auxdecor< float >( "B_mass3" ));
  if( eventInfo->isAvailable< int >( "B_matched3" ) )
    m_BMatched.push_back(eventInfo->auxdecor< int >( "B_matched3" ));
  if( eventInfo->isAvailable< float >( "B_nearest3" ) )
    m_BNearest.push_back(eventInfo->auxdecor< float >( "B_nearest3" ));

  if( eventInfo->isAvailable< int >( "B_index4" ) )
    m_BIndex.push_back(eventInfo->auxdecor< int >( "B_index4" ));
  if( eventInfo->isAvailable< float >( "B_pt4" ) )
    m_BPt.push_back(eventInfo->auxdecor< float >( "B_pt4" ));
  if( eventInfo->isAvailable< float >( "B_eta4" ) )
    m_BEta.push_back(eventInfo->auxdecor< float >( "B_eta4" ));
  if( eventInfo->isAvailable< float >( "B_phi4" ) )
    m_BPhi.push_back(eventInfo->auxdecor< float >( "B_phi4" ));
  if( eventInfo->isAvailable< float >( "B_mass4" ) )
    m_BMass.push_back(eventInfo->auxdecor< float >( "B_mass4" ));
  if( eventInfo->isAvailable< int >( "B_matched4" ) )
    m_BMatched.push_back(eventInfo->auxdecor< int >( "B_matched4" ));
  if( eventInfo->isAvailable< float >( "B_nearest4" ) )
    m_BNearest.push_back(eventInfo->auxdecor< float >( "B_nearest4" ));

  if( eventInfo->isAvailable< int >( "passesFilter" ) )
    m_passesFilter = eventInfo->auxdecor< int >( "passesFilter" );
  if( eventInfo->isAvailable< int >( "passesElecFilter" ) )
    m_passesElecFilter = eventInfo->auxdecor< int >( "passesElecFilter" );
  if( eventInfo->isAvailable< int >( "passesMuonFilter" ) )
    m_passesMuonFilter = eventInfo->auxdecor< int >( "passesMuonFilter" );
  if( eventInfo->isAvailable< float >( "Met" ) )
    m_Met = eventInfo->auxdecor< float >( "Met" );
  if( eventInfo->isAvailable< float >( "Met_Phi" ) )
    m_Met_Phi = eventInfo->auxdecor< float >( "Met_Phi" );
  if( eventInfo->isAvailable< float >( "Met_Sumet" ) )
    m_Met_Sumet = eventInfo->auxdecor< float >( "Met_Sumet" );

  if( eventInfo->isAvailable< float >( "lepJetMinCHFDeltaPhi" ) )
    m_Lep_JetMinCHF_DeltaPhi = eventInfo->auxdecor< float >( "lepJetMinCHFDeltaPhi" );
  if( eventInfo->isAvailable< float >( "lepSumLeadingJetsDeltaPhi" ) )
    m_Lep_SumLeadingJets_DeltaPhi = eventInfo->auxdecor< float >( "lepSumLeadingJetsDeltaPhi" );
  if( eventInfo->isAvailable< float >( "vecJetMinCHFDeltaPhi" ) )
    m_Vec_JetMinCHF_DeltaPhi = eventInfo->auxdecor< float >( "vecJetMinCHFDeltaPhi" );
  if( eventInfo->isAvailable< float >( "vecSumLeadingJetsDeltaPhi" ) )
    m_Vec_SumLeadingJets_DeltaPhi = eventInfo->auxdecor< float >( "vecSumLeadingJetsDeltaPhi" );
  if( eventInfo->isAvailable< float >( "metDPhi" ) )
    m_metDeltaPhi = eventInfo->auxdecor< float >( "metDPhi" );


  if( eventInfo->isAvailable< float >( "weight" ) )
    m_weight = eventInfo->auxdecor< float >( "weight" );
  if( eventInfo->isAvailable< float >( "weight_xs" ) )
    m_weight_xs = eventInfo->auxdecor< float >( "weight_xs" );
  if( eventInfo->isAvailable< float >( "weight_prescale" ) )
    m_weight_prescale = eventInfo->auxdecor< float >( "weight_prescale" );
  if( eventInfo->isAvailable< float >( "weight_resonanceKFactor" ) )
    m_weight_resonanceKFactor = eventInfo->auxdecor< float >( "weight_resonanceKFactor" );

}

/////////////////// Assign values to defined jet variables here //////////////////
void MiniTree::FillJetsUser( const xAOD::Jet* jet, const std::string jetName ) {

  if( jet->isAvailable< float >( "alpha_max" ) ) {
    m_jet_alpha_max.push_back( jet->auxdata< float >("alpha_max") );
  } else {
    m_jet_alpha_max.push_back( -999 );
  }
  if( jet->isAvailable< int >( "alpha_max_pass" ) ) {
    m_jet_alpha_max_pass.push_back( jet->auxdata< int >("alpha_max_pass") );
  } else {
    m_jet_alpha_max_pass.push_back( -999 );
  }
  if( jet->isAvailable< float >( "chf" ) ) {
    m_jet_chf.push_back( jet->auxdata< float >("chf") );
  } else {
    m_jet_chf.push_back( -999 );
  }
  if( jet->isAvailable< int >( "chf_pass" ) ) {
    m_jet_chf_pass.push_back( jet->auxdata< int >("chf_pass") );
  } else {
    m_jet_chf_pass.push_back( -999 );
  }
  if( jet->isAvailable< float >( "track_pt_max" ) ) {
    m_jet_track_pt_max.push_back( jet->auxdata< float >("track_pt_max") );
  } else {
    m_jet_track_pt_max.push_back( -999 );
  }
  if( jet->isAvailable< int >( "ntracks" ) ) {
    m_jet_ntracks.push_back( jet->auxdata< int >("ntracks") );
  } else {
    m_jet_ntracks.push_back( -999 );
  }
  if( jet->isAvailable< float >( "LooseDR" ) ) {
    m_jet_LooseDR.push_back( jet->auxdata< float >("LooseDR") );
  } else {
    m_jet_LooseDR.push_back( -999 );
  }
  if( jet->isAvailable< float >( "MediumDR" ) ) {
    m_jet_MediumDR.push_back( jet->auxdata< float >("MediumDR") );
  } else {
    m_jet_MediumDR.push_back( -999 );
  }
  if( jet->isAvailable< float >( "TightDR" ) ) {
    m_jet_TightDR.push_back( jet->auxdata< float >("TightDR") );
  } else {
    m_jet_TightDR.push_back( -999 );
  }
  if( jet->isAvailable< float >( "LooseDPhi" ) ) {
    m_jet_LooseDPhi.push_back( jet->auxdata< float >("LooseDPhi") );
  } else {
    m_jet_LooseDPhi.push_back( -999 );
  }
  if( jet->isAvailable< float >( "MediumDPhi" ) ) {
    m_jet_MediumDPhi.push_back( jet->auxdata< float >("MediumDPhi") );
  } else {
    m_jet_MediumDPhi.push_back( -999 );
  }
  if( jet->isAvailable< float >( "TightDPhi" ) ) {
    m_jet_TightDPhi.push_back( jet->auxdata< float >("TightDPhi") );
  } else {
    m_jet_TightDPhi.push_back( -999 );
  }
  if( jet->isAvailable< int >( "LLP_index" ) ) {
    m_jet_LLP_index.push_back( jet->auxdata< int >("LLP_index") );
  } else {
    m_jet_LLP_index.push_back( -999 );
  }
  if( jet->isAvailable< int >( "numB" ) ) {
    m_jet_num_b.push_back( jet->auxdata< int >("numB") );
  } else {
    m_jet_num_b.push_back( -999 );
  }
  if( jet->isAvailable< float >( "nearestB" ) ) {
    m_jet_nearest_b.push_back( jet->auxdata< float >("nearestB") );
  } else {
    m_jet_nearest_b.push_back( -999 );
  }
}


/////////////////// Assign values to defined electron variables here //////////////////                                                                                                 
void MiniTree::FillElectronsUser( const xAOD::Electron* elec, const std::string elecName ) {
  if( elec->isAvailable< int >( "isGood" ) ) {
    m_el_isGood.push_back( elec->auxdata< int >("isGood") );
  } else {
    m_el_isGood.push_back( -999 );
  }
}

void MiniTree::FillTracksUser( const std::string trackName, const xAOD::TrackParticle* track) {
  if( track->isAvailable< int >( "isAssoc" ) ) {
    m_track_isAssoc.push_back( track->auxdata< int >("isAssoc") );
  } else {
    m_track_isAssoc.push_back( -999 );
  }
  if( track->isAvailable< int >( "jetIndex" ) ) {
    m_track_jetIndex.push_back( track->auxdata< int >("jetIndex") );
  } else {
    m_track_jetIndex.push_back( -999 );
  }
}
