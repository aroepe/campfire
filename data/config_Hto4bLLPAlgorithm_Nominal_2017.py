from xAODAnaHelpers import Config

c = Config()

#%%%%%%%%%%%%%%%%%%%%%%%%%% BasicEventSelection %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("BasicEventSelection",    { 
  "m_name"                      : "BasicEventSelect",
  "m_applyGRLCut"               : False,
  "m_GRLxml"                    : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20180906/physics_25ns_Triggerno17e33prim.xml",
  #"m_derivationName"            : "SUSY15Kernel_skim",
  "m_useMetaData"               : False,
  "m_storePassHLT"              : True,
  "m_storeTrigDecisions"        : True,
  "m_storePassL1"	        : True,
  "m_storeTrigKeys" 	        : True,
  "m_applyTriggerCut"           : True,
  "m_doPUreweighting"           : False,
  "m_PRWFileNames"              : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/PRW_AUTOCONFIG/files/pileup_mc16d_dsid361021.root,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/PRW_AUTOCONFIG/files/pileup_mc16c_dsid361021.root",
  "m_lumiCalcFileNames"         : "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/ilumicalc_histograms_None_276262-284154.root,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/ilumicalc_histograms_None_297730-299243.root,/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/ilumicalc_histograms_None_325713-338349_OflLumi-13TeV-001.root",
  "m_autoconfigPRW"             : False,
  #"m_triggerSelection"          : "HLT_j85_boffperf || HLT_j150_2j50 || HLT_2j25 || HLT_2j65_boffperf_split_j65 || HLT_2j65_boffperf_j65 || HLT_2j65_bperf_j65 || HLT_2j225_gsc250_boffperf_split_0eta240_j70_gsc120_boffperf_split_0eta240",
  "m_triggerSelection"          : "HLT_mu26_ivarmedium || HLT_e140_lhloose_nod0 || HLT_e60_lhmedium_nod0 || HLT_e26_lhtight_nod0_ivarloose",
  "m_checkDuplicatesData"       : False,
  "m_applyEventCleaningCut"     : False,
  "m_applyCoreFlagsCut"	        : False,
  "m_vertexContainerName"       : "PrimaryVertices",
  "m_applyPrimaryVertexCut"     : False, 
  "m_PVNTrack"		        : 2,
  "m_msgLevel"                  : "Info",
})

##%%%%%%%%%%%%%%%%%%%%%%%%%%% Hto4bFilter%%%%%%%%%%%%%%%%%%%%%%%%%%%#                                                                                                               
                                                                                                                                                                                    
c.algorithm("Hto4bLLPFilter",       {                                                                                                                                               
    "m_name"                    : "Hto4bLLPFilter",                                                                                                                                 
    #----------------------- Container Flow ----------------------------#                                                                                                           
                                                                                                                                                                                    
    "m_allJetContainerName"     : "AntiKt4EMTopoJets",                                                                                                                              
    "m_inMuContainerName"       : "Muons",                                                                                                                                          
    "m_inElContainerName"       : "Electrons",                                                                                                                                      
    #----------------------- Selections ----------------------------#                                                                                                               
                                                                                                                                                                                    
    "m_TrackMinPt"              : 400,                                                                                                                                              
    "m_TrackZ0Max"              : 0.3,                                                                                                                                              
    "m_TrackD0Max"              : 0.5,                                                                                                                                              
    "m_jetPtCut"                : 20,                                                                                                                                               
    "m_AlphaMaxCut"             : 0.03,                                                                                                                                             
    "m_CHFCut"                  : 0.3,                                                                                                                                              
    "m_electronPtCut"           : 27000,                                                                                                                                            
    "m_muonPtCut"               : 25000,                                                                                                                                            
    #----------------------- Other ----------------------------#                                                                                                            
                                                                                                                                                                                  
    "m_msgLevel"                : "Info",                                                                                                                                         
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% METTrk Constructor %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("METConstructor", {
  "m_name"                      : "MetTrkConstruct",
  "m_referenceMETContainer"     : "MET_Reference_AntiKt4EMTopo",
  "m_mapName"                   : "METAssoc_AntiKt4EMTopo",
  "m_coreName"                  : "MET_Core_AntiKt4EMTopo",
  "m_outputContainer"           : "METTrk",
  "m_outputAlgoSystNames"       : "METTrk_Syst",
  "m_writeSystToMetadata"       : False,
  "m_setAFII"                   : True,
  "m_calculateSignificance"     : True,
  "m_doPhotonCuts"              : True,
  "m_doElectronCuts"            : True,
  "m_addSoftClusterTerms"       : False,
  "m_rebuildUsingTracksInJets"  : True,
  "m_inputElectrons"            : "Electrons",
  "m_inputMuons"                : "Muons",
  #"m_inputTaus"                 : "TauJets",
  "m_inputJets"                 : "AntiKt4EMTopoJets",
  "m_runNominal"                : True,
  #"m_eleSystematics"            : "ElectronSelector_Syst",
  #"m_muonSystematics"           : "MuonSelector_Syst",
  #"m_tauSystematics"            : "TauSelector_Syst",
  #"m_jetSystematics"            : "JetSelector_Syst",
  "m_doJVTCut"                  : True,
  "m_dofJVTCut"                 : False,
  "m_calculateSignificance"     : False,
  "m_msgLevel"                  : "Info"
})

#%%%%%%%%%%%%%%%%%%%%%%%%%% MET Constructor %%%%%%%%%%%%%%%%%%%%%%%%%%#                                                                                                              
c.algorithm("METConstructor", {
  "m_name"                      : "MetConstruct",
  "m_referenceMETContainer"     : "MET_Reference_AntiKt4EMTopo",
  "m_mapName"                   : "METAssoc_AntiKt4EMTopo",
  "m_coreName"                  : "MET_Core_AntiKt4EMTopo",
  "m_outputContainer"           : "MET",
  "m_outputAlgoSystNames"       : "MET_Syst",
  "m_writeSystToMetadata"       : False,
  "m_setAFII"                   : True,
  "m_calculateSignificance"     : True,
  "m_doPhotonCuts"              : True,
  "m_doElectronCuts"            : True,
  "m_addSoftClusterTerms"       : False,
  "m_rebuildUsingTracksInJets"  : False,
  "m_inputElectrons"            : "Electrons",
  "m_inputMuons"                : "Muons",
  #"m_inputTaus"                 : "TauJets",
  "m_inputJets"                 : "AntiKt4EMTopoJets",
  "m_runNominal"                : True,
  #"m_eleSystematics"            : "ElectronSelector_Syst",
  #"m_muonSystematics"           : "MuonSelector_Syst",
  #"m_tauSystematics"            : "TauSelector_Syst",
  #"m_jetSystematics"            : "JetSelector_Syst",
  "m_doJVTCut"                  : True,
  "m_dofJVTCut"                 : False,
  "m_calculateSignificance"     : False,
  "m_msgLevel"                  : "Info"
})

##%%%%%%%%%%%%%%%%%%%%%%%%%% Hto4bLLPAlgo %%%%%%%%%%%%%%%%%%%%%%%%%%#
c.algorithm("Hto4bLLPAlgorithm",     {
    "m_name"                    : "Hto4bLLPAlgo",
    #----------------------- Container Flow ----------------------------#
    "m_inJetContainerName"      : "SignalJets",
    "m_inputAlgo"               : "SignalJets_Algo",
    "m_allJetContainerName"     : "AntiKt4EMTopoJets_Calib",
    "m_allJetInputAlgo"         : "AntiKt4EMTopoJets_Calib_Algo",
    "m_inMuContainerName"       : "Muons_Signal",
    "m_inElContainerName"       : "Electrons_Signal",
    "m_inMETContainerName"      : "MET",
    "m_inMETTrkContainerName"   : "METTrk",
    #----------------------- Selections ----------------------------#
    "m_leadingJetPtCut"         : 20,
    "m_subleadingJetPtCut"      : 20,
    "m_jetMultiplicity"         : 2,
    "m_useMCPileupCheck"        : False,
    "m_metCut"                  : 20000,
    #----------------------- Other ----------------------------# 
    "m_MCPileupCheckContainer"  : "AntiKt4TruthJets",
    "m_msgLevel"                : "Info",
})

##%%%%%%%%%%%%%%%%%%%%%%%%Hto4bNtuple###############################  
c.algorithm("Hto4bLLPNtuple",     {
    "m_name"                    : "Hto4bLLPNtuple",
    #----------------------- Container Flow ----------------------------#
    "m_inJetContainerName"      : "AntiKt4EMTopoJets",
    "m_inputAlgo"               : "",#"SignalJets_Algo",
    "m_allJetContainerName"     : "AntiKt4EMTopoJets",
    "m_allJetInputAlgo"         : "",#"AntiKt4EMTopoJets_Calib_Algo",
    "m_inMuContainerName"       : "Muons",
    "m_inElContainerName"       : "Electrons",
    "m_inMETContainerName"      : "MET",
    "m_inMETTrkContainerName"   : "METTrk",
    #----------------------- Output ----------------------------#
    "m_eventDetailStr"          : "truth pileup", #shapeEM                                                                                                                          
    "m_jetDetailStr"            : "kinematic rapidity clean energy truth flavorTag trackAll trackPV allTrackPVSel allTrackDetail allTrackDetailPVSel btag_jettrk",
    "m_jetDetailStrSyst"        : "kinematic rapidity energy clean flavorTag",
    "m_elDetailStr"             : "kinematic clean energy truth flavorTag isolation trackparams trackhitcont effSF PID", #trigger 
    "m_muDetailStr"             : "kinematic clean energy truth flavorTag isolation trackparams trackhitcont effSF quality energyLoss", #trigger
    "m_trigDetailStr"           : "basic passTriggers",#basic menuKeys passTriggers",
    "m_metDetailStr"            : "metClus",
    "m_metTrkDetailStr"         : "metTrk",
    "m_trackDetailStr"          : "fitpars",
    #----------------------- Other ----------------------------#
    "m_useMCPileupCheck"        : False,
    "m_MCPileupCheckContainer"  : "AntiKt4TruthJets",
    "m_msgLevel"                : "Info",
})
