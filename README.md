This is the event selection code for the VH->XX->4b ID analysis and is written for use with SUSY15 DAODs.



The following is the recipe to use in order to run the code.

For first time setup:


    mkdir CAMPFIRE; cd CAMPFIRE
    mkdir src build run
    cd src
    asetup AnalysisBase,21.2.19,here
    git clone https://gitlab.cern.ch/aroepe/campfire.git
    git clone https://github.com/UCATLAS/xAODAnaHelpers.git
    cd xAODAnaHelpers
    git checkout 87fc46de064ecaee3a9df18d37062a60dbab6879
    cd ../campfire
    git checkout -b [your branch name]
    cd ../../build
    cmake ../src
    make -j
    cd ..
    source build/${CMTCONFIG}/setup.sh
    cd run


Otherwise:


    cd CAMPFIRE/src
    asetup AnalysisBase,21.2.19,here
    cd ..
    source build/${CMTCONFIG}/setup.sh
    cd run



Now that the environment is setup, you can run the xAH_run.py software in the run directory.

An example of how to run this at the command line is as follows:

    xAH_run.py --config ../src/campfire/data/config_Hto4bLLPAlgorithm_Nominal_2017.py --files [path to your file] --isMC --submitDir rel21_mc --nevents 100000 direct

This example, in particular, is for a Monte Carlo sample because it has the --isMC option added.

More information on the options for xAH_run.py can be found here: https://xaodanahelpers.readthedocs.io/en/latest/UsingUs.html#api-reference



