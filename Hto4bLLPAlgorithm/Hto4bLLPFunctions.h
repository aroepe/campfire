#ifndef Hto4bLLPFunctions_H
#define Hto4bLLPFunctions_H

// Framework include(s)
#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "TH1D.h"
#include "TFile.h"
#include "TEnv.h"
#include "TSystem.h"

#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticleContainer.h"


namespace Hto4bLLPFunctions {

  typedef std::vector<const xAOD::TrackParticle*> Particles;
  typedef ElementLink<xAOD::TrackParticleContainer> TrackLink;
  typedef std::vector<TrackLink> TrackLinks;

  Particles btagAssociatedTracks(const xAOD::Jet* jet, int jetIndex);
  bool isB(const xAOD::TruthParticle* p);
  std::vector<const xAOD::TruthParticle*> getBDecayChain(const xAOD::TruthParticle* p);
  EL::StatusCode passCut(TH1D* cutflowHist, TH1D* cutflowHistW, int iCutflow, float eventWeight);

}

#endif
