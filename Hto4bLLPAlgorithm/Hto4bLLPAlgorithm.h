#ifndef Hto4bLLPAlgorithm_H
#define Hto4bLLPAlgorithm_H

// Framework include(s)
#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include <xAODAnaHelpers/JetHists.h>
#include "Hto4bLLPAlgorithm/MiniTree.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

// ROOT include(s):
#include "TH1D.h"

#include <sstream>

class Hto4bLLPAlgorithm : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:

    //configuration variables
    std::string m_inJetContainerName;   // input Jet container name
    std::string m_inputAlgo;          // input algo for when running systs
    std::string m_allJetContainerName;// input container name
    std::string m_allJetInputAlgo;    // input algo for when running systs
    std::string m_inMETContainerName;  // input MET container name
    std::string m_inMETTrkContainerName;  // input METTrk container name 
    std::string m_inMuContainerName;    // input Muon container name
    std::string m_inElContainerName;    // input Electron container name
    bool m_isMC;                      // Is MC
    bool m_useCutFlow;                // true will write out cutflow histograms
    std::string m_MCPileupCheckContainer; // Name of truth container for MC Pileup Check
    bool m_useMCPileupCheck;          // determined by name of MCPileupCheckContainer
    float m_leadingJetPtCut;          // Leading jet Pt cut
    float m_subleadingJetPtCut;          // Leading jet Pt cut
    uint m_jetMultiplicity;          // Leading jet Pt cut
    bool m_truthLevelOnly;            // truthLevelOnly info
    float m_metCut;


  private:
    int m_eventCounter;     //!

    TH1D* m_cutflowHist;    //!
    TH1D* m_cutflowHistW;   //!
    int m_cutflowFirst;     //!
    int m_iCutflow;         //!
    float m_mcEventWeight;  //!
    std::string m_comEnergy; //!

    float m_xs; //!
    float m_filtEff; //!
    int m_numAMIEvents; //!
    int m_mcChannelNumber; //!
    std::stringstream m_ss; //!

    std::string m_treeStream;

    EL::StatusCode getLumiWeights(const xAOD::EventInfo* eventInfo);
    std::map< std::string, JetHists*> m_jetHists; //!
    std::map< std::string, MiniTree* > m_myTrees; //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // this is a standard constructor
  Hto4bLLPAlgorithm ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // these are the functions not inherited from Algorithm
  virtual EL::StatusCode configure ();
  bool executeAnalysis( const xAOD::EventInfo* eventInfo,
  const xAOD::JetContainer* signalJets,
  const xAOD::JetContainer* truthJets,
  const xAOD::JetContainer* allJets,
  const xAOD::MuonContainer* allMuons,
  const xAOD::ElectronContainer* allElectrons,    
  const xAOD::VertexContainer* vertices,
  const xAOD::TrackParticleContainer* tracks,
  const xAOD::TruthParticleContainer* TruthPart,
  const xAOD::MissingETContainer* Met,
  const xAOD::MissingETContainer* MetTrk,
  bool count,
  std::string systName = "");
  
  void AddTree( std::string );
  void AddHists( std::string );

  void newJetContainers(const xAOD::JetContainer* input, const xAOD::ElectronContainer* electrons);
  void newElectronContainers(const xAOD::ElectronContainer* input, const xAOD::EventInfo* eventInfo, const xAOD::VertexContainer* vertices);
  void newMuonContainers(const xAOD::MuonContainer* input, const xAOD::EventInfo* eventInfo, const xAOD::VertexContainer* vertices);

  // this is needed to distribute the algorithm to the workers
  ClassDef(Hto4bLLPAlgorithm, 1);
};

#endif
