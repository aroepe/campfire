#ifndef AnalysisExample_MiniTree_H
#define AnalysisExample_MiniTree_H

#include "xAODAnaHelpers/HelpTreeBase.h"
#include "TTree.h"

class MiniTree : public HelpTreeBase
{

  private:
    bool m_firstEvent;

    float m_mjj;
    float m_m3j;
    float m_mnj;

    std::vector< float > m_LLP_beta;
    std::vector< float > m_LLP_gamma;
    std::vector< float > m_LLP_R_coord;
    std::vector< float > m_LLP_X_coord;
    std::vector< float > m_LLP_Y_coord;
    std::vector< float > m_LLP_Z_coord;
    std::vector< float > m_LLP_lifetime_dil;
    std::vector< float > m_LLP_lifetime_prop;
    std::vector< float > m_LLP_Pt;
    std::vector< float > m_LLP_Eta;
    std::vector< float > m_LLP_Phi;
    std::vector< float > m_LLP_Mass;
    std::vector< int >   m_BIndex;
    std::vector< float > m_BPt;
    std::vector< float > m_BEta;
    std::vector< float > m_BPhi;
    std::vector< float > m_BMass;
    std::vector< int >   m_BMatched;
    std::vector< float > m_BNearest;
    int m_passesFilter;
    int m_passesElecFilter;
    int m_passesMuonFilter;
    float m_Met;
    float m_Met_Phi;
    float m_Met_Sumet;
    float m_Lep_JetMinCHF_DeltaPhi;
    float m_Lep_SumLeadingJets_DeltaPhi;
    float m_Vec_JetMinCHF_DeltaPhi;
    float m_Vec_SumLeadingJets_DeltaPhi;
    float m_metDeltaPhi;
    int m_Vchild;
    float m_VchildPt;
    int m_VId;
    float m_VPt;
    float m_VEta;
    float m_VPhi;
    float m_VMass;
    int m_NeutrinoId;
    float m_NeutrinoPt;
    float m_NeutrinoEta;
    float m_NeutrinoPhi;
    float m_NeutrinoMass;
    float m_TrackMet;
    float m_HPt;
    float m_HEta;
    float m_HPhi;
    float m_HMass;

    float m_weight;
    float m_weight_corr;
    float m_weight_xs;
    float m_weight_prescale;
    float m_weight_resonanceKFactor;

    std::vector<float> m_jet_alpha_max;
    std::vector<int> m_jet_alpha_max_pass;
    std::vector<float> m_jet_chf;
    std::vector<int> m_jet_chf_pass;
    std::vector<float> m_jet_track_pt_max;
    std::vector<int> m_jet_ntracks;
    std::vector<float> m_jet_LooseDR;
    std::vector<float> m_jet_MediumDR;
    std::vector<float> m_jet_TightDR;
    std::vector<float> m_jet_LooseDPhi;
    std::vector<float> m_jet_MediumDPhi;
    std::vector<float> m_jet_TightDPhi;
    std::vector<int> m_jet_LLP_index;
    std::vector<int> m_jet_num_b;
    std::vector<float> m_jet_nearest_b;

    std::vector<int> m_el_isGood;

    std::vector<int> m_track_isAssoc;
    std::vector<int> m_track_jetIndex;

  public:

    MiniTree(xAOD::TEvent * event, TTree* tree, TFile* file, xAOD::TStore* store = nullptr);
    ~MiniTree();

    void AddEventUser( const std::string detailStr = "" );
    void AddJetsUser( const std::string detailStr = "" , const std::string jetName = "jet");
    void AddElectronsUser( const std::string detailStr = "");
    void AddTracksUser( const std::string trackName="track", const std::string detailStr="");
    void FillEventUser( const xAOD::EventInfo* eventInfo );
    void FillJetsUser( const xAOD::Jet* jet, const std::string jetName = "jet" );
    void FillElectronsUser( const xAOD::Electron*, const std::string elecName = "el" );
    void FillTracksUser( const std::string trackName="track", const xAOD::TrackParticle* track=nullptr);
    void ClearEventUser();
    void ClearJetsUser( const std::string jetName = "jet");
    void ClearElectronsUser( const std::string elecName = "el");
    void ClearTracksUser ( const std::string trackName = "track");


};
#endif
