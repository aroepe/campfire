#ifndef Hto4bLLPAlgorithm_Hto4bLLPFilter_H
#define Hto4bLLPAlgorithm_Hto4bLLPFilter_H

// Framework include(s)
#include <EventLoop/StatusCode.h>
#include <EventLoop/Algorithm.h>

//algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include <xAODAnaHelpers/JetHists.h>
#include "xAODTracking/TrackParticleContainer.h"

#include "AsgTools/AnaToolHandle.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"


class Hto4bLLPFilter : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:

    //configuration variables
    std::string m_allJetContainerName;// input container name
    std::string m_inMuContainerName;    // input Muon container name
    std::string m_inElContainerName;    // input Electron container name
    float m_TrackMinPt;              //minimum track pt for jet_alpha_max and jet_chf
    float m_TrackZ0Max;              //maximum z0sintheta for jet_alpha_max
    float m_TrackD0Max;              //maximum d0 for jet_alpha_max and jet_chf
    float m_jetPtCut;
    float m_AlphaMaxCut;
    float m_CHFCut;
    float m_electronPtCut;
    float m_muonPtCut;

  private:
    int m_eventCounter;     //!
    asg::AnaToolHandle<CP::IMuonSelectionTool>       m_muonSelectionTool_handle     {"CP::MuonSelectionTool/MuonSelectionTool"          , this};
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:

  // this is a standard constructor
  Hto4bLLPFilter ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // these are the functions not inherited from Algorithm
  virtual EL::StatusCode configure ();

  bool executeFilter( const xAOD::EventInfo* eventInfo,
		      const xAOD::JetContainer* allJets,
		      const xAOD::MuonContainer* allMuons,
		      const xAOD::ElectronContainer* allElectrons,    
		      const xAOD::VertexContainer* vertices);
  
  void newJetContainers(const xAOD::JetContainer* input, 
			const xAOD::ElectronContainer* electrons);

  void newElectronContainers(const xAOD::ElectronContainer* input, 
			     const xAOD::EventInfo* eventInfo, 
			     const xAOD::VertexContainer* vertices);

  void newMuonContainers(const xAOD::MuonContainer* input, 
			 const xAOD::EventInfo* eventInfo, 
			 const xAOD::VertexContainer* vertices);

  // this is needed to distribute the algorithm to the workers
  ClassDef(Hto4bLLPFilter, 1);
};

#endif
